#ifndef fakeFrontEndData_h
#define fakeFrontEndData_h

#include "share_var.h"

typedef struct
{
    int redEnhancement_fake[ProbCace_NUMBER][NURSE_ARRAY_SUPPORT][WORKDAY_ARRAY_SUPPORT];
    int leave_fake[ProbCace_NUMBER][NURSE_ARRAY_SUPPORT][WORKDAY_ARRAY_SUPPORT];
    int resultLastMonth_fake[NURSE_ARRAY_SUPPORT][LAST_6WORKDAY_LAST_MONTH];
    int OwingLeave_fake[NURSE_ARRAY_SUPPORT];
    int AnnualLeave_fake[NURSE_ARRAY_SUPPORT];
    int AnnualLeaveComplete_fake[NURSE_ARRAY_SUPPORT];
    int quota_fake[NURSE_ARRAY_SUPPORT];
    int jobgrade_fake[ProbCace_NUMBER][NURSE_ARRAY_SUPPORT];
    int lastmonth_shift_fake[NURSE_ARRAY_SUPPORT];
    int longterm_leave_fake[ProbCace_NUMBER][NURSE_ARRAY_SUPPORT][WORKDAY_ARRAY_SUPPORT];
    int green_leave_fake[ProbCace_NUMBER][NURSE_ARRAY_SUPPORT][WORKDAY_ARRAY_SUPPORT];
}FakeData_t;

void data_parser(int problem_idx);
void SetFakeFrontEndDataIntoShare_var(int problem_idx, FakeData_t* fake);
void setResultLastMonth(int* resultLastMonthData);
void setMonthAsJuly(Shift_t *shift);
void init_personnel_requirement(Shift_t* shift);
void modify_personnel_requirement(Shift_t* shift);
void setTenStaffs(Staff_t *staffs, int problem_idx, FakeData_t* fake);
void setWeekendinfo(Shift_t *shift);
void initFakeData(FakeData_t* fake);

#endif
