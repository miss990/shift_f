//
//  shift.c
//  shift
//
//  Created by 張柏勛 on 2019/6/1.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#include "Woodcarving.h"
#include <stdio.h>
#include <stdlib.h>
#include "glpk.h"

int Woodcarving(void)
{
    printf("Start GLPK\n");
    glp_prob *lp;//Problem objects
    int ia[1+1000], ja[1+1000];
    double ar[1+1000], z, x1, x2;
    lp = glp_create_prob();//create problem object
    glp_set_prob_name(lp, "Woodcarving");//assign (change) problem name
    glp_set_obj_dir(lp, GLP_MAX);//set (change) optimization direction flag
    
    glp_add_rows(lp, 3);//add new rows (constraints) to problem object
    glp_set_row_name(lp, 1, "finishing");//assign (change) row name to i-th row (auxiliary variable)
    glp_set_row_bnds(lp, 1, GLP_UP, 0.0, 100.0);//set (change) row bounds of i-th row (auxiliary variable)
    glp_set_row_name(lp, 2, "carpentry");
    glp_set_row_bnds(lp, 2, GLP_UP, 0.0, 80.0);
    glp_set_row_name(lp, 3, "demand");
    glp_set_row_bnds(lp, 3, GLP_UP, 0.0, 40.0);
    
    glp_add_cols(lp, 2);//add new columns (structural variables) to problem object
    glp_set_col_name(lp, 1, "x1");//assign (change) column name to j-th column (structural variable)
    glp_set_col_bnds(lp, 1, GLP_LO, 0.0, 0.0);//set (change) column bounds of j-th column (structural variable)
    glp_set_obj_coef(lp, 1, 3.0); //set (change) objective coefficient or constant term at j-th column (structural variable)
    glp_set_col_name(lp, 2, "x2");
    glp_set_col_bnds(lp, 2, GLP_LO, 0.0, 0.0);
    glp_set_obj_coef(lp, 2, 2.0);
    
    ia[1] = 1; ja[1] = 1; ar[1] = 2.0;  /* a[1,1] = 2 */
    ia[2] = 1; ja[2] = 2; ar[2] = 1.0;  /* a[1,2] = 1 */
    ia[3] = 2; ja[3] = 1; ar[3] = 1.0;  /* a[2,1] = 1 */
    ia[4] = 2; ja[4] = 2; ar[4] = 1.0;  /* a[2,2] = 1 */
    ia[5] = 3; ja[5] = 1; ar[5] = 1.0;  /* a[3,1] = 1 */
    //ia[6] = 3; ja[6] = 2; ar[6] = 0.0;  /* a[3,2] = 0 */
    
    glp_load_matrix(lp, 5, ia, ja, ar);//load (replace) the whole constraint matrix, where ia[k] is the row index, ja[k] is the column index, and ar[k] is a numeric value of corresponding constraint coefficient
    glp_simplex(lp, NULL);//solve LP problem with the primal or dual simplex method
    z = glp_get_obj_val(lp);//retrieve objective value
    x1 = glp_get_col_prim(lp, 1);//retrieve column primal value
    x2 = glp_get_col_prim(lp, 2);
    printf("\nz = %g; x1 = %g; x2 = %g\n", z, x1, x2);
    glp_delete_prob(lp);//erase problem object content
    return 0;
}

