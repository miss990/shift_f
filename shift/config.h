//
//  config.h
//  shift
//
//  Created by 張柏勛 on 2019/6/4.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#ifndef config_h
#define config_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//#define GLPSOL
#define GLPSOL_PATH                          ("/usr/local/bin/glpsol -m ")
#define GLPSOL_FILE                          ("shift_")

#define MAX_INT                              (0x7FFFFFFF)
#define MIN_INT                              (0x80000000)
#define MSP_ASK_CNT_PORTION                  (1)
#define MSP_REMAIN_OWING_PORTION             (1)
#define MSP_REMAIN_ANNUAL_PORTION            (1)
#define MSP_TOTAL_PORTION                    (MSP_ASK_CNT_PORTION + MSP_REMAIN_OWING_PORTION + MSP_REMAIN_ANNUAL_PORTION)
#define STRATEGY_CNT                         (4)
#define SOLUTION_ITEM_VALUE_CNT              (2)
#define NURSE_ARRAY_SUPPORT                  (25)
#define WORKDAY_ARRAY_SUPPORT                (31)
#define SCORE_TABLE_ASKING                   (7)
#define SCORE_TABLE_VARIABLE                 (6)
#define CONSTANT_ONE                         (1)
#define NO_ASK_RED_ENHANCEMENT               (-1)

#define MAX_TOTAL_SHIFT                      (3)
#define LAST_6WORKDAY_LAST_MONTH             (6)
#define ON_DUTY_CNT                          (3)
#define STAND_BY_CNT                         (1)
#define OFF_DUTY_CNT                         (4)
#define MID_SHIFT_CNT                        (1)
#define WEEKEND_CNT                          (11)
#define HOURS_PER_DAY                        (8)
#define RUNTIME_LIMIT_SEC                    (60 * 1000)
#define OBJECTIVE_DIRECTION                  ("maximize")

typedef enum
{
    ProbCase_LARGE,
    ProbCase_SMALL_SEP,
    ProbCase_SMALL_MAR,
    ProbCase_SMALL_JUN_D,
    ProbCase_SMALL_JUN_N,
    ProbCase_SMALL_MAR21_D,
    ProbCase_SMALL_MAR21_N,
    ProbCase_SMALL_MAR21_G,
    ProbCace_NUMBER
}ProbCase_t;

typedef enum
{
    ProbCfg_TOTAL_SHIFT,
    ProbCfg_SHIFT_DAY_NURSE,
    ProbCfg_SHIFT_NIGHT_NURSE,
    ProbCfg_SHIFT_GRAVEYARD_NURSE,
    ProbCfg_TOTAL_NURSE,
    ProbCfg_TOTAL_WORKDAY,
    ProbCfg_LAST_MONTH_P_TOTAL_WORKDAY,
    ProbCfg_FIRSTDAY_IS,
    ProbCfg_SHIFT_CATEGORY,
    ProbCfg_MIN_STAFF_PER_WEEKDAY_DAY,
    ProbCfg_MIN_STAFF_PER_HOLIDAY_DAY,
    ProbCfg_MIN_STAFF_PER_WEEKDAY_NIGHT,
    ProbCfg_MIN_STAFF_PER_HOLIDAY_NIGHT,
    ProbCfg_MIN_STAFF_PER_WEEKDAY_GRAVEYARD,
    ProbCfg_MIN_STAFF_PER_HOLIDAY_GRAVEYARD,
    ProbCfg_MIN_LEADER_PER_WORKDAY_DAY,
    ProbCfg_MIN_LEADER_PER_WORKDAY_NIGHT,
    ProbCfg_MIN_LEADER_PER_WORKDAY_GRAVEYARD,
    ProbCfg_MIN_CHARGE_PER_WORKDAY_DAY,
    ProbCfg_MIN_CHARGE_PER_WORKDAY_NIGHT,
    ProbCfg_MIN_CHARGE_PER_WORKDAY_GRAVEYARD,
    ProbCfg_CONT_ON_DUTY_LIMIT_DAY_SHIFT,
    ProbCfg_CONT_ON_DUTY_LIMIT_NIGHT_SHIFT,
    ProbCfg_CONT_ON_DUTY_LIMIT,
    ProbCfg_MIN_OFF_DUTY_PER_STAFF,
    ProbCfg_MAX_OFF_DUTY_PER_STAFF,
    ProbCfg_MIN_ON_DUTY_DAYS_NIGHT_SHIFT,
    ProbCfg_RED_ENHANCEMENT_CONSTANT,
    ProbCfg_STB_ENABLE,
    ProbCfg_STB_POOL_DAY,
    ProbCfg_STB_POOL_NIGHT,
    ProbCfg_STB_POOL_GRAVEYARD,
    ProbCfg_MID_SHIFT_ENABLE,
    ProbCfg_MID_POOL_DAY,
    ProbCfg_MID_POOL_NIGHT,
    ProbCfg_MID_POOL_GRAVEYARD,
    ProbCfg_N4_CHARGE,
    ProbCfg_N4_LEADER,
    ProbCfg_N4_STAFF,
    ProbCfg_N4_STAND_BY,
    ProbCfg_N4_MID_SHIFT,
    ProbCfg_N3_CHARGE,
    ProbCfg_N3_LEADER,
    ProbCfg_N3_STAFF,
    ProbCfg_N3_STAND_BY,
    ProbCfg_N3_MID_SHIFT,
    ProbCfg_N2_CHARGE,
    ProbCfg_N2_LEADER,
    ProbCfg_N2_STAFF,
    ProbCfg_N2_STAND_BY,
    ProbCfg_N2_MID_SHIFT,
    ProbCfg_N1_CHARGE,
    ProbCfg_N1_LEADER,
    ProbCfg_N1_STAFF,
    ProbCfg_N1_STAND_BY,
    ProbCfg_N1_MID_SHIFT,
    ProbCfg_N0_CHARGE,
    ProbCfg_N0_LEADER,
    ProbCfg_N0_STAFF,
    ProbCfg_N0_STAND_BY,
    ProbCfg_N0_MID_SHIFT,
    ProbCfg_NUMBER
}ProbCfg_t;

typedef enum
{
    POSITION_STAFF,
    POSITION_LEADER,
    POSITION_CHARGE,
    POSITION_STAND_BY,
    POSITION_MID_SHIFT,
    POSITION_NUMBER
}JobPosition_t;

typedef enum
{
    GRADE_N0, //staff, standby, mid
    GRADE_N1, //leader, staff, standby, mid
    GRADE_N2, //charge, leader, staff, mid
    GRADE_N3, //charge, leader
    GRADE_N4, //charge
    GRADE_NUMBER
}JobGrade_t;

typedef enum
{
    SHIFT_DAY,
    SHIFT_NIGHT,
    SHIFT_GRAVEYARD,
    SHIFT_NUMBER
}ShiftCategory_t;

typedef struct
{
    int total_shift;
    int shift_category;
    int shift_nurse[3];
    int first_day_is;
    int total_nurse;
    int total_workday;
    int on_duty_cnt;
    int stand_by_cnt;
    int off_duty_cnt;
    int mid_shift_cnt;
    int valid_status_cnt;
    int last_6workday_last_month;
    int last_month_p_total_workday;
    int red_enhancement_constant;
    ShiftCategory_t min_staff_per_weekday[3];
    ShiftCategory_t min_staff_per_holiday[3];
    ShiftCategory_t min_leader_per_workday[3];
    ShiftCategory_t min_charge_per_workday[3];
    ShiftCategory_t stand_by_pool[3];
    ShiftCategory_t mid_shift_pool[3];
    bool grade_fit_position[GRADE_NUMBER][POSITION_NUMBER][SHIFT_NUMBER];
    int cont_on_duty_limit_day_shift;
    int cont_on_duty_limit_night_shift;
    int cont_on_duty_limit;
    int max_off_duty_per_staff;
    int min_off_duty_per_staff;
    int min_on_duty_days_night_shift;
    bool stand_by_bool;
    bool mid_shift_bool;
    int total_variable;
}Config_t;

typedef enum{//小心！在不任意中斷連貫性前提下，可新增HRSTATUS
    HRSTATUS_UNDEFINED               = -1,
    ///////// On Duty Set //////////
    HRSTATUS_CHARGE_ONDUTY_DAY       =  0,
    HRSTATUS_LEADER_ONDUTY_DAY       =  1,
    HRSTATUS_STAFF_ONDUTY_DAY        =  2,
    HRSTATUS_STANDBY_DAY             =  3,
    ////////////////////////////////
    HRSTATUS_CHARGE_ONDUTY_NIGHT     =  4,
    HRSTATUS_LEADER_ONDUTY_NIGHT     =  5,
    HRSTATUS_STAFF_ONDUTY_NIGHT      =  6,
    HRSTATUS_STANDBY_NIGHT           =  7,
    ////////////////////////////////
    HRSTATUS_CHARGE_ONDUTY_GRAVEYARD =  8,
    HRSTATUS_LEADER_ONDUTY_GRAVEYARD =  9,
    HRSTATUS_STAFF_ONDUTY_GRAVEYARD  = 10,
    HRSTATUS_STANDBY_GRAVEYARD       = 11,
    ///////// Off Duty Set //////////
    HRSTATUS_RED_OFFDUTY             = 12,
    HRSTATUS_RED_ENHANCE_OFFDUTY     = 13,
    HRSTATUS_GREEN_OFFDUTY           = 14,
    HRSTATUS_LONG_OFFDUTY            = 15,
    /////////// Mid Set ////////////
    HRSTATUS_MID_ONDUTY              = 16,
    HRSTATUS_NUMBER
}HRStatus_t;

typedef enum{
    ASKSTATUS_NON = 0,
    ASKSTATUS_RED_ENHANCEMENT_OFFDUTY,
    ASKSTATUS_RED_OFFDUTY,
    ASKSTATUS_GREEN_OFFDUTY,
    ASKSTATUS_LONG_OFFDUTY,
    ASKSTATUS_NUMBER
}AskStatus_t;

typedef enum
{
    ROW_UNIQUE_STATUS,
    ROW_STAFF_DEMAND,
    ROW_LEADER_DEMAND,
    ROW_CHARGE_DEMAND,
    ROW_POSITION_LIMIT,
    ROW_SHIFT_LIMIT,
    ROW_LEAVE_QUOTA,
    ROW_LONG_TERM_LEAVE,
    ROW_GREEN_LINE_LEAVE,
    ROW_STAND_BY_DEMAND,
    ROW_STAND_BY_LEVELING,
    ROW_STAND_BY_LIMIT,
    ROW_MID_SHIFT_DEMAND,
    ROW_MID_SHIFT_LEVELING,
    ROW_MID_SHIFT_LIMIT,
    ROW_CONTINUOUS_ON_DUTY,
    ROW_OFF_DUTY_SEPARATE,
    ROW_MIN_OFF_DUTY_DAY,
    ROW_WEEKEND_LEVELING,
    ROW_SHIFT_TO_SHIFT,
    ROW_NIGHT_SHIFT_15DAY,
    ROW_NUMBER
}GlpRowConstraint_t;

typedef enum
{
    WEEK_MONDAY = 1,
    WEEK_TUESDAY,
    WEEK_WEDNESDAY,
    WEEK_THURSDAY,
    WEEK_FRIDAY,
    WEEK_SATURDAY,
    WEEK_SUNDAY
}Week_t;

typedef enum
{
    MSP_ASK_CNT = 0,
    MSP_REMAIN_OWING,
    MSP_REMAIN_ANNUAL,
    MSP_TOTAL
}ModifiedScoreParameter_t;

extern int strategy_idx;
void config_setting(int problem_idx);
void config_delete(void);
void config_setting_with_shift(ShiftCategory_t shift, int problem_idx);
Config_t* createEmptyConfig(void);

int get_config_total_shift(void);
int get_config_shift_category(void);
int get_config_first_day(void);
int* get_config_shift_nurse(void);
int get_config_first_day_is(void);
int get_config_total_nurse(void);
int get_config_total_workday(void);
int get_config_on_duty_cnt(void);
int get_config_stand_by_cnt(void);
int get_config_off_duty_cnt(void);
int get_config_mid_shift_cnt(void);
int get_config_valid_status_cnt(void);
int get_config_last_6workday_last_month(void);
int get_config_last_month_p_total_workday(void);
ShiftCategory_t* get_config_min_staff_per_weekday(void);
ShiftCategory_t* get_config_min_staff_per_holiday(void);
ShiftCategory_t* get_config_min_leader_per_workday(void);
ShiftCategory_t* get_config_min_charge_per_workday(void);
ShiftCategory_t* get_config_stand_by_pool(void);
ShiftCategory_t* get_config_mid_shift_pool(void);
int get_config_cont_on_duty_limit_day_shift(void);
int get_config_cont_on_duty_limit_night_shift(void);
//int get_config_cont_on_duty_limit(void);
int get_config_max_off_duty_per_staff(void);
int get_config_min_off_duty_per_staff(void);
int get_config_min_on_duty_days_night_shift(void);
int get_config_stand_by_bool(void);
int get_config_mid_shift_bool(void);
int get_config_total_variable_cnt(void);
int get_config_red_enhancement_constant(void);
bool* get_config_grade_fit_position(void);

bool check_grade_not_meet_any_position(JobGrade_t grade, ShiftCategory_t shift);

static inline void set_config_total_shift(int value);
static inline void set_config_shift_category(int value);
static inline void set_config_first_day(int value);
static inline void set_config_shift_nurse(int day, int night, int graveyard);
static inline void set_config_first_day_is(int value);
static inline void set_config_total_nurse(int value);
static inline void set_config_total_workday(int value);
static inline void set_config_on_duty_cnt(int value);
static inline void set_config_stand_by_cnt(int value);
static inline void set_config_off_duty_cnt(int value);
static inline void set_config_mid_shift_cnt(int value);
static inline void set_config_valid_status_cnt(void);
static inline void set_config_last_6workday_last_month(int value);
static inline void set_config_last_month_p_total_workday(void);
static inline void set_config_min_staff_per_weekday(int day, int night, int graveyard);
static inline void set_config_min_staff_per_holiday(int day, int night, int graveyard);
static inline void set_config_min_leader_per_workday(int day, int night, int graveyard);
static inline void set_config_min_charge_per_workday(int day, int night, int graveyard);
static inline void set_config_stand_by_pool(bool day, bool night, bool graveyard);
static inline void set_config_mid_shift_pool(bool day, bool night, bool graveyard);
static inline void set_config_cont_on_duty_limit_day_shift(int value);
static inline void set_config_cont_on_duty_limit_night_shift(int value);
static inline void set_config_cont_on_duty_limit(int value);
static inline void set_config_max_off_duty_per_staff(int value);
static inline void set_config_min_off_duty_per_staff(int value);
static inline void set_config_min_on_duty_days_night_shift(int value);
static inline void set_config_stand_by_bool(int value);
static inline void set_config_mid_shift_bool(int value);
static inline void set_config_total_variable_cnt(void);
static inline void set_config_red_enhancement_constant(int value);
static inline void set_config_grade_fit_position_set(ShiftCategory_t shift, int problem_idx);
static inline void set_config_grade_fit_position(JobGrade_t grade, bool charge, bool leader, bool staff, bool stand_by,
                                                 bool mid_shift, ShiftCategory_t shift);

#endif /* config_h */
