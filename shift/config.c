//
//  config.c
//  shift
//
//  Created by 張柏勛 on 2019/9/3.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#include "config.h"
#include "memory.h"

static Config_t *config;
int problem_config[ProbCfg_NUMBER][ProbCace_NUMBER] =
{
/*TOTAL_SHIFT*/                        {1,                3,                 3,              1,              1,             1,               1,               1},
/*SHIFT_DAY_NURSE*/                    {25,               12,                12,             16,             0,             18,              0,               0},
/*SHIFT_NIGHT_NURSE*/                  {0,                5,                 5,              0,              14,            0,               17,              0},
/*SHIFT_GRAVEYARD_NURSE*/              {0,                3,                 3,              0,              0,             0,               0,               17},
/*TOTAL_NURSE*/                        {25+0+0,           12+5+3,            12+5+3,         16+0+0,         0+14+0,        18,              17,              17},
/*TOTAL_WORKDAY*/                      {30,               30,                31,             30,             30,            30,              30,              30},
/*LAST_MONTH_P_T_DAY*/{30+LAST_6WORKDAY_LAST_MONTH,       30+LAST_6WORKDAY_LAST_MONTH,       31+LAST_6WORKDAY_LAST_MONTH,   30+LAST_6WORKDAY_LAST_MONTH,  30+LAST_6WORKDAY_LAST_MONTH,
    30+LAST_6WORKDAY_LAST_MONTH,  30+LAST_6WORKDAY_LAST_MONTH,  30+LAST_6WORKDAY_LAST_MONTH},
/*FIRSTDAY_IS*/                        {WEEK_SATURDAY,    WEEK_SUNDAY,       WEEK_FRIDAY,    WEEK_MONDAY,    WEEK_MONDAY,   WEEK_THURSDAY,   WEEK_THURSDAY,   WEEK_THURSDAY},
/*SHIFT_CATEGORY*/                     {SHIFT_DAY,        SHIFT_NUMBER,      SHIFT_NUMBER,   SHIFT_DAY,      SHIFT_NIGHT,   SHIFT_DAY,       SHIFT_NIGHT,     SHIFT_GRAVEYARD},
/*MIN_STAFF_PER_WEEKDAY_DAY*/          {13,               6,                 6,              7,              0,             10,              0,               0},
/*MIN_STAFF_PER_HOLIDAY_DAY*/          {13,               5,                 5,              7,              0,             9,              0,               0},
/*MIN_STAFF_PER_WEEKDAY_NIGHT*/        {0,                3,                 3,              0,              7,             0,               10,              0},
/*MIN_STAFF_PER_HOLIDAY_NIGHT*/        {0,                3,                 3,              0,              7,             0,               10,              0},
/*MIN_STAFF_PER_WEEKDAY_GRAVEYARD*/    {0,                2,                 2,              0,              0,             0,               0,               10},
/*MIN_STAFF_PER_HOLIDAY_GRAVEYARD*/    {0,                2,                 2,              0,              0,             0,               0,               10},
/*MIN_LEADER_PER_WORKDAY_DAY*/         {2,                0,                 0,              2,              0,             2,               0,               0},
/*MIN_LEADER_PER_WORKDAY_NIGHT*/       {0,                0,                 0,              0,              2,             0,               2,               0},
/*MIN_LEADER_PER_WORKDAY_GRAVEYARD*/   {0,                0,                 0,              0,              0,             0,               0,               2},
/*MIN_CHARGE_PER_WORKDAY_DAY*/         {1,                1,                 1,              1,              0,             1,               0,               0},
/*MIN_CHARGE_PER_WORKDAY_NIGHT*/       {0,                0,                 0,              0,              0,             0,               0,               0},
/*MIN_CHARGE_PER_WORKDAY_GRAVEYARD*/   {0,                0,                 0,              0,              0,             0,               0,               0},
/*CONT_ON_DUTY_LIMIT_DAY_SHIFT*/       {5,                5,                 5,              5,              5,             6,               5,               5},
/*CONT_ON_DUTY_LIMIT_NIGHT_SHIFT*/     {5,                5,                 5,              5,              5,             5,               5,               5},
/*CONT_ON_DUTY_LIMIT*/                 {5,                5,                 5,              5,              5,             6,               5,               5},
/*MIN_OFF_DUTY_PER_STAFF*/             {10,               10,                10,             10,             10,            8,               8,               8},
/*MAX_OFF_DUTY_PER_STAFF*/             {12,               13,                13,             13,             13,            10,              12,              12},
/*MIN_ON_DUTY_DAYS_NIGHT_SHIFT*/       {15,               15,                15,             15,             15,            15,              15,              15},
/*RED_ENHANCEMENT_COEFF*/              {18,               18,                18,             18,             18,            18,              18,              18},
/*STB_ENABLE*/                         {true,             false,             false,          true,           true,          false,           false,           false},
/*STB_POOL_DAY*/                       {true,             false,             false,          true,           false,         false,           false,           false},
/*STB_POOL_NIGHT*/                     {false,            false,             false,          false,          true,          false,           false,           false},
/*STB_POOL_GRAVEYARD*/                 {false,            false,             false,          false,          false,         false,           false,           false},
/*MID_SHIFT_ENABLE*/                   {false,            true,              true,           false,          false,         false,           false,           false},
/*MID_POOL_DAY*/                       {false,            true,              true,           false,          false,         false,           false,           false},
/*MID_POOL_NIGHT*/                     {false,            true,              true,           false,          false,         false,           false,           false},
/*MID_POOL_GRAVEYARD*/                 {false,            false,             false,          false,          false,         false,           false,           false},

/*N4_CHARGE*/                          {true,             true,              true,           true,           true,          true,            true,            true},
/*N4_LEADER*/                          {false,            false,             false,          false,          false,         true,           false,           false},
/*N4_STAFF*/                           {false,            false,             false,          false,          false,         false,           false,           false},
/*N4_STAND_BY*/                        {false,            false,             false,          false,          false,         false,           false,           false},
/*N4_MID_SHIFT*/                       {false,            false,             false,          false,          false,         false,           false,           false},

/*N3_CHARGE*/                          {true,             true,              true,           true,           true,          true,            true,            true},
/*N3_LEADER*/                          {true,             true,              true,           true,           true,          true,            true,            true},
/*N3_STAFF*/                           {false,            false,             false,          false,          false,         true,            true,            true},
/*N3_STAND_BY*/                        {false,            false,             false,          false,          false,         false,           false,           false},
/*N3_MID_SHIFT*/                       {false,            false,             false,          false,          false,         false,           false,           false},

/*N2_CHARGE*/                          {true,             true,              true,           true,           true,          true,            true,            true},
/*N2_LEADER*/                          {true,             true,              true,           true,           true,          true,            true,            true},
/*N2_STAFF*/                           {true,             true,              true,           true,           true,          true,            true,            true},
/*N2_STAND_BY*/                        {false,            false,             false,          true,           false,         false,           false,           false},
/*N2_MID_SHIFT*/                       {true,             true,              true,           true,           true,          false,           false,           false},

/*N1_CHARGE*/                          {false,            false,             false,          false,          false,         false,           false,           false},
/*N1_LEADER*/                          {true,             true,              true,           true,           true,          true,            true,            true},
/*N1_STAFF*/                           {true,             true,              true,           true,           true,          true,            true,            true},
/*N1_STAND_BY*/                        {true,             true,              true,           true,           true,          false,           false,           false},
/*N1_MID_SHIFT*/                       {true,             true,              true,           true,           true,          false,           false,           false},

/*N0_CHARGE*/                          {false,            false,             false,          false,          false,         false,           false,           false},
/*N0_LEADER*/                          {false,            false,             false,          false,          false,         false,           false,           false},
/*N0_STAFF*/                           {true,             true,              true,           true,           true,          true,            true,            true},
/*N0_STAND_BY*/                        {true,             true,              true,           true,           true,          false,           false,           false},
/*N0_MID_SHIFT*/                       {true,             true,              true,           true,           true,          false,           false,           false}
};
int backup_problem_config[ProbCfg_NUMBER][ProbCace_NUMBER];

int modifyStrategy[STRATEGY_CNT][SOLUTION_ITEM_VALUE_CNT] =
{
    {                                   0,  0},
    {ProbCfg_CONT_ON_DUTY_LIMIT_DAY_SHIFT,  1},
    {ProbCfg_MIN_OFF_DUTY_PER_STAFF,       -1},
    {ProbCfg_MIN_ON_DUTY_DAYS_NIGHT_SHIFT, -1}
};

Config_t* createEmptyConfig(void)
{
    Config_t* config = (Config_t *)malloc(sizeof(Config_t));//1
    return config;
}

static inline void backup_restore_config(void)
{
    if(strategy_idx == 0)
        memcpy(backup_problem_config, problem_config, sizeof(problem_config));
    else
        memcpy(problem_config, backup_problem_config, sizeof(problem_config));
}

static inline void modify_config_during_loops(int problem_idx)
{
    int modifyItem  = modifyStrategy[strategy_idx][0];
    int modifyValue = modifyStrategy[strategy_idx][1];
    problem_config[modifyItem][problem_idx] += modifyValue;
}

static inline void set_config_total_shift(int value)
{
    config->total_shift = value;
}

static inline void set_config_shift_category(int value)
{
    config->shift_category = value;
}

static inline void set_config_shift_nurse(int day, int night, int graveyard)
{
    config->shift_nurse[SHIFT_DAY]       = day;
    config->shift_nurse[SHIFT_NIGHT]     = night;
    config->shift_nurse[SHIFT_GRAVEYARD] = graveyard;
}

static inline void set_config_first_day_is(int value)
{
    config->first_day_is = value;
}

static inline void set_config_total_nurse(int value)
{
    config->total_nurse = value;
}

static inline void set_config_total_workday(int value)
{
    config->total_workday = value;
}

static inline void set_config_on_duty_cnt(int value)
{
    config->on_duty_cnt = value;
}

static inline void set_config_stand_by_cnt(int value)
{
    config->stand_by_cnt = value;
}

static inline void set_config_off_duty_cnt(int value)
{
    config->off_duty_cnt = value;
}

static inline void set_config_mid_shift_cnt(int value)
{
    config->mid_shift_cnt = value;
}

static inline void set_config_valid_status_cnt(void)
{
    int total_shift  = get_config_total_shift();
    int on_duty_cnt  = get_config_on_duty_cnt();
    int stand_by_cnt = get_config_stand_by_cnt();
    int off_duty_cnt = get_config_off_duty_cnt();
    config->valid_status_cnt = total_shift * (on_duty_cnt + stand_by_cnt) + off_duty_cnt;
}

static inline void set_config_last_6workday_last_month(int value)
{
    config->last_6workday_last_month = value;
}

static inline void set_config_last_month_p_total_workday(void)
{
    config->last_month_p_total_workday = config->last_6workday_last_month + config->total_workday;
}

static inline void set_config_min_personnel(int problem_idx)
{
    set_config_min_staff_per_weekday (problem_config[ProbCfg_MIN_STAFF_PER_WEEKDAY_DAY][problem_idx],
                                      problem_config[ProbCfg_MIN_STAFF_PER_WEEKDAY_NIGHT][problem_idx],
                                      problem_config[ProbCfg_MIN_STAFF_PER_WEEKDAY_GRAVEYARD][problem_idx]);
    
    set_config_min_staff_per_holiday (problem_config[ProbCfg_MIN_STAFF_PER_HOLIDAY_DAY][problem_idx],
                                      problem_config[ProbCfg_MIN_STAFF_PER_HOLIDAY_NIGHT][problem_idx],
                                      problem_config[ProbCfg_MIN_STAFF_PER_HOLIDAY_GRAVEYARD][problem_idx]);
    
    set_config_min_leader_per_workday(problem_config[ProbCfg_MIN_LEADER_PER_WORKDAY_DAY][problem_idx],
                                      problem_config[ProbCfg_MIN_LEADER_PER_WORKDAY_NIGHT][problem_idx],
                                      problem_config[ProbCfg_MIN_LEADER_PER_WORKDAY_GRAVEYARD][problem_idx]);
    
    set_config_min_charge_per_workday(problem_config[ProbCfg_MIN_CHARGE_PER_WORKDAY_DAY][problem_idx],
                                      problem_config[ProbCfg_MIN_CHARGE_PER_WORKDAY_NIGHT][problem_idx],
                                      problem_config[ProbCfg_MIN_CHARGE_PER_WORKDAY_GRAVEYARD][problem_idx]);
}

static inline void set_config_min_staff_per_weekday(int day, int night, int graveyard)
{
    config->min_staff_per_weekday[SHIFT_DAY]       = day;
    config->min_staff_per_weekday[SHIFT_NIGHT]     = night;
    config->min_staff_per_weekday[SHIFT_GRAVEYARD] = graveyard;
}

static inline void set_config_min_staff_per_holiday(int day, int night, int graveyard)
{
    config->min_staff_per_holiday[SHIFT_DAY]       = day;
    config->min_staff_per_holiday[SHIFT_NIGHT]     = night;
    config->min_staff_per_holiday[SHIFT_GRAVEYARD] = graveyard;
}

static inline void set_config_min_leader_per_workday(int day, int night, int graveyard)
{
    config->min_leader_per_workday[SHIFT_DAY]       = day;
    config->min_leader_per_workday[SHIFT_NIGHT]     = night;
    config->min_leader_per_workday[SHIFT_GRAVEYARD] = graveyard;
}

static inline void set_config_min_charge_per_workday(int day, int night, int graveyard)
{
    config->min_charge_per_workday[SHIFT_DAY]       = day;
    config->min_charge_per_workday[SHIFT_NIGHT]     = night;
    config->min_charge_per_workday[SHIFT_GRAVEYARD] = graveyard;
}

static inline void set_config_stand_by_pool(bool day, bool night, bool graveyard)
{
    config->stand_by_pool[SHIFT_DAY]       = day;
    config->stand_by_pool[SHIFT_NIGHT]     = night;
    config->stand_by_pool[SHIFT_GRAVEYARD] = graveyard;
}

static inline void set_config_mid_shift_pool(bool day, bool night, bool graveyard)
{
    config->mid_shift_pool[SHIFT_DAY]       = day;
    config->mid_shift_pool[SHIFT_NIGHT]     = night;
    config->mid_shift_pool[SHIFT_GRAVEYARD] = graveyard;
}

static inline void set_config_grade_fit_position_set(ShiftCategory_t shift, int problem_idx)
{
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    if(shift != 3)
    {
        shift_s = shift;
        shift_e = shift_s + 1;
    }
    printf("shift %d\n", shift);
    memset(&config->grade_fit_position, false, sizeof(config->grade_fit_position));
    
    ShiftCategory_t* stdpool = get_config_stand_by_pool();
    ShiftCategory_t* midpool = get_config_mid_shift_pool();
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        set_config_grade_fit_position(GRADE_N4, problem_config[ProbCfg_N4_CHARGE][problem_idx],
                                      problem_config[ProbCfg_N4_LEADER][problem_idx],
                                      problem_config[ProbCfg_N4_STAFF][problem_idx],
                                      problem_config[ProbCfg_N4_STAND_BY][problem_idx] && stdpool[shift_c],
                                      problem_config[ProbCfg_N4_MID_SHIFT][problem_idx] && midpool[shift_c], shift_c);
        
        set_config_grade_fit_position(GRADE_N3, problem_config[ProbCfg_N3_CHARGE][problem_idx],
                                      problem_config[ProbCfg_N3_LEADER][problem_idx],
                                      problem_config[ProbCfg_N3_STAFF][problem_idx],
                                      problem_config[ProbCfg_N3_STAND_BY][problem_idx] && stdpool[shift_c],
                                      problem_config[ProbCfg_N3_MID_SHIFT][problem_idx] && midpool[shift_c], shift_c);
        
        set_config_grade_fit_position(GRADE_N2, problem_config[ProbCfg_N2_CHARGE][problem_idx],
                                      problem_config[ProbCfg_N2_LEADER][problem_idx],
                                      problem_config[ProbCfg_N2_STAFF][problem_idx],
                                      problem_config[ProbCfg_N2_STAND_BY][problem_idx] && stdpool[shift_c],
                                      problem_config[ProbCfg_N2_MID_SHIFT][problem_idx] && midpool[shift_c], shift_c);
        
        set_config_grade_fit_position(GRADE_N1, problem_config[ProbCfg_N1_CHARGE][problem_idx],
                                      problem_config[ProbCfg_N1_LEADER][problem_idx],
                                      problem_config[ProbCfg_N1_STAFF][problem_idx],
                                      problem_config[ProbCfg_N1_STAND_BY][problem_idx] && stdpool[shift_c],
                                      problem_config[ProbCfg_N1_MID_SHIFT][problem_idx] && midpool[shift_c], shift_c);
        
        set_config_grade_fit_position(GRADE_N0, problem_config[ProbCfg_N0_CHARGE][problem_idx],
                                      problem_config[ProbCfg_N0_LEADER][problem_idx],
                                      problem_config[ProbCfg_N0_STAFF][problem_idx],
                                      problem_config[ProbCfg_N0_STAND_BY][problem_idx] && stdpool[shift_c],
                                      problem_config[ProbCfg_N0_MID_SHIFT][problem_idx] && midpool[shift_c], shift_c);
    }
}

static inline void set_config_grade_fit_position(JobGrade_t grade, bool charge, bool leader, bool staff, bool stand_by,
                                                 bool mid_shift, ShiftCategory_t shift)
{
    config->grade_fit_position[grade][POSITION_CHARGE][shift]    = charge;
    config->grade_fit_position[grade][POSITION_LEADER][shift]    = leader;
    config->grade_fit_position[grade][POSITION_STAFF][shift]     = staff;
    config->grade_fit_position[grade][POSITION_STAND_BY][shift]  = stand_by;
    config->grade_fit_position[grade][POSITION_MID_SHIFT][shift] = mid_shift;
}

static inline void set_config_cont_on_duty_limit_day_shift(int value)
{
    config->cont_on_duty_limit_day_shift = value;
}

static inline void set_config_cont_on_duty_limit_night_shift(int value)
{
    config->cont_on_duty_limit_night_shift = value;
}

static inline void set_config_cont_on_duty_limit(int value)
{
    config->cont_on_duty_limit = value;
}

static inline void set_config_max_off_duty_per_staff(int value)
{
    config->max_off_duty_per_staff = value;
}

static inline void set_config_min_off_duty_per_staff(int value)
{
    config->min_off_duty_per_staff = value;
}

static inline void set_config_min_on_duty_days_night_shift(int value)
{
    config->min_on_duty_days_night_shift = value;
}

static inline void set_config_stand_by_bool(int value)
{
    config->stand_by_bool = value;
}

static inline void set_config_mid_shift_bool(int value)
{
    config->mid_shift_bool = value;
}

static inline void set_config_total_variable_cnt(void)
{
    int total_nurse = get_config_total_nurse();
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    config->total_variable = (total_nurse * last_month_p_total_workday * HRSTATUS_NUMBER);
}

static inline void set_config_red_enhancement_constant(int value)
{
    config->red_enhancement_constant = value;
}

void config_setting(int problem_idx)
{
    //config = createEmptyConfig();
    config = (Config_t *)calloc(1, sizeof(Config_t));//1
    backup_restore_config();
    modify_config_during_loops(problem_idx);
    set_config_total_shift(problem_config[ProbCfg_TOTAL_SHIFT][problem_idx]);
    set_config_shift_category(problem_config[ProbCfg_SHIFT_CATEGORY][problem_idx]);
    set_config_shift_nurse(problem_config[ProbCfg_SHIFT_DAY_NURSE][problem_idx],
                           problem_config[ProbCfg_SHIFT_NIGHT_NURSE][problem_idx],
                           problem_config[ProbCfg_SHIFT_GRAVEYARD_NURSE][problem_idx]);
    
    set_config_first_day_is(problem_config[ProbCfg_FIRSTDAY_IS][problem_idx]);
    set_config_total_nurse(problem_config[ProbCfg_TOTAL_NURSE][problem_idx]);
    set_config_total_workday(problem_config[ProbCfg_TOTAL_WORKDAY][problem_idx]);
    set_config_on_duty_cnt(ON_DUTY_CNT);
    set_config_stand_by_cnt(STAND_BY_CNT);
    set_config_off_duty_cnt(OFF_DUTY_CNT);
    set_config_mid_shift_cnt(MID_SHIFT_CNT);
    set_config_valid_status_cnt();
    set_config_last_6workday_last_month(LAST_6WORKDAY_LAST_MONTH);
    set_config_last_month_p_total_workday();
    set_config_min_personnel(problem_idx);
    set_config_stand_by_pool(problem_config[ProbCfg_STB_POOL_DAY][problem_idx],
                             problem_config[ProbCfg_STB_POOL_NIGHT][problem_idx],
                             problem_config[ProbCfg_STB_POOL_GRAVEYARD][problem_idx]);
    
    set_config_mid_shift_pool(problem_config[ProbCfg_MID_POOL_DAY][problem_idx],
                              problem_config[ProbCfg_MID_POOL_NIGHT][problem_idx],
                              problem_config[ProbCfg_MID_POOL_GRAVEYARD][problem_idx]);
    
    set_config_cont_on_duty_limit_day_shift  (problem_config[ProbCfg_CONT_ON_DUTY_LIMIT_DAY_SHIFT][problem_idx]);
    set_config_cont_on_duty_limit_night_shift(problem_config[ProbCfg_CONT_ON_DUTY_LIMIT_NIGHT_SHIFT][problem_idx]);
    set_config_cont_on_duty_limit            (problem_config[ProbCfg_CONT_ON_DUTY_LIMIT][problem_idx]);
    set_config_max_off_duty_per_staff        (problem_config[ProbCfg_MAX_OFF_DUTY_PER_STAFF][problem_idx]);
    set_config_min_off_duty_per_staff        (problem_config[ProbCfg_MIN_OFF_DUTY_PER_STAFF][problem_idx]);
    set_config_min_on_duty_days_night_shift  (problem_config[ProbCfg_MIN_ON_DUTY_DAYS_NIGHT_SHIFT][problem_idx]);
    set_config_stand_by_bool                 (problem_config[ProbCfg_STB_ENABLE][problem_idx]);
    set_config_mid_shift_bool                (problem_config[ProbCfg_MID_SHIFT_ENABLE][problem_idx]);
    set_config_total_variable_cnt();
    set_config_red_enhancement_constant(problem_config[ProbCfg_RED_ENHANCEMENT_CONSTANT][problem_idx]);
}

void config_delete(void)
{
    if(config != NULL)
    {
        free(config);//1.a
        config = NULL;
    }
}

void config_setting_with_shift(ShiftCategory_t shift, int problem_idx)
{
    set_config_grade_fit_position_set(shift, problem_idx);
}

int get_config_total_shift(void)
{
    return config->total_shift;
}

int get_config_shift_category(void)
{
    return config->shift_category;
}

int* get_config_shift_nurse(void)
{
    return config->shift_nurse;
}

int get_config_first_day_is(void)
{
    return config->first_day_is;
}

int get_config_total_nurse(void)
{
    return config->total_nurse;
}

int get_config_total_workday(void)
{
    return config->total_workday;
}

int get_config_on_duty_cnt(void)
{
    return config->on_duty_cnt;
}

int get_config_stand_by_cnt(void)
{
    return config->stand_by_cnt;
}

int get_config_off_duty_cnt(void)
{
    return config->off_duty_cnt;
}

int get_config_mid_shift_cnt(void)
{
    return config->mid_shift_cnt;
}

int get_config_valid_status_cnt(void)
{
    return config->valid_status_cnt;
}

int get_config_last_6workday_last_month(void)
{
    return config->last_6workday_last_month;
}

int get_config_last_month_p_total_workday(void)
{
    return config->last_month_p_total_workday;
}

ShiftCategory_t* get_config_min_staff_per_weekday(void)
{
    return &config->min_staff_per_weekday[0];
}

ShiftCategory_t* get_config_min_staff_per_holiday(void)
{
    return &config->min_staff_per_holiday[0];
}

ShiftCategory_t* get_config_min_leader_per_workday(void)
{
    return &config->min_leader_per_workday[0];
}

ShiftCategory_t* get_config_min_charge_per_workday(void)
{
    return &config->min_charge_per_workday[0];
}

ShiftCategory_t* get_config_stand_by_pool(void)
{
    return &config->stand_by_pool[0];
}

ShiftCategory_t* get_config_mid_shift_pool(void)
{
    return &config->mid_shift_pool[0];
}

bool* get_config_grade_fit_position(void)
{
    return &config->grade_fit_position[0][0][0];
}

int get_config_cont_on_duty_limit_day_shift(void)
{
    return config->cont_on_duty_limit_day_shift;
}

int get_config_cont_on_duty_limit_night_shift(void)
{
    return config->cont_on_duty_limit_night_shift;
}

//int get_config_cont_on_duty_limit(void)
//{
//    return config->cont_on_duty_limit;
//}

int get_config_max_off_duty_per_staff(void)
{
    return config->max_off_duty_per_staff;
}

int get_config_min_off_duty_per_staff(void)
{
    return config->min_off_duty_per_staff;
}

int get_config_min_on_duty_days_night_shift(void)
{
    return config->min_on_duty_days_night_shift;
}

int get_config_stand_by_bool(void)
{
    return config->stand_by_bool;
}

int get_config_mid_shift_bool(void)
{
    return config->mid_shift_bool;
}

int get_config_total_variable_cnt(void)
{
    return config->total_variable;
}

int get_config_red_enhancement_constant(void)
{
    return config->red_enhancement_constant;
}

bool check_grade_not_meet_any_position(JobGrade_t grade, ShiftCategory_t shift)
{
    return !(config->grade_fit_position[grade][POSITION_CHARGE][shift] &&
            config->grade_fit_position[grade][POSITION_LEADER][shift] &&
            config->grade_fit_position[grade][POSITION_STAFF][shift] &&
            config->grade_fit_position[grade][POSITION_STAND_BY][shift]);
}
