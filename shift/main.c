//
//  main.c
//  shift
//
//  Created by 張柏勛 on 2019/6/1.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include "Woodcarving.h"
#include "gen_gmpl_file.h"
#include "gen_glp_problem.h"
#include "fakeFrontEndData.h"
#include "Results.h"
//#include "omp.h"
#include <sys/time.h>

extern FILE* fout;
int strategy_idx=0;
extern tCONSTRAINT *backup_row_constraint_table;

#ifdef GLPSOL
void gmpl_sol(char* filemod, char* filesol)
{
    strcpy(filesol, filemod);
    strcat(filesol, ".sol");
}

void gmpl_cmd(int problem_idx, char* exe, char* filemod, char* filesol)
{
    char idx[] = "0";
    sprintf(idx, "%d", problem_idx);
    strcat(filemod, idx);
    gmpl_sol(filemod, filesol);
    strcat(filemod, ".mod");
    strcat(exe, filemod);
    strcat(exe, " --output ");
    strcat(exe, filesol);
}

void gmpl_method(int problem_idx, char* filesol)
{
    char exe[80] = GLPSOL_PATH;
    char filemod[80] = GLPSOL_FILE;
    gmpl_cmd(problem_idx, exe, filemod, filesol);
    
    if( access(filemod, 0 ) == 0 )
    {
        remove(filemod);
        printf("shift exist and been deleted\n");
    }
#ifndef PRINT
    fout = fopen(filemod, "a+");
#endif
    
    gen_gmpl_file();
    
#ifndef PRINT
    fclose(fout);
#endif
    printf("Execute %s\n", exe);
    system(exe);
}

void show_gmpl_sol(char* filesol)
{
    char head[30] = "head ";
    strcat(head, filesol);
    system(head);
}
#endif

void gpointer_delete(void)
{
    int total_nurse = get_config_total_nurse();
    for(int nurse=0; nurse<total_nurse; nurse++)
    {
        memoryRelease(staffs[nurse].LMShiftList);//6.a
        memoryRelease(staffs[nurse].AskLeaveList);//5.a
    }
    memoryRelease(staffs);//4.a
    
    memoryRelease(shift);//3.a
}

int main(int argc, const char * argv[])
{
    printf("Hello, World!\n");
    struct timeval t1, t2;
    double elapsedTime;
    // start timer
    gettimeofday(&t1, NULL);
    
    allResult_init();
#ifdef GLPSOL
    char filesol[ProbCace_NUMBER][80];
#endif
    //for(int caseIndex=ProbCace_NUMBER-4; caseIndex<ProbCace_NUMBER-2; caseIndex++)
    //for(int caseIndex=2; caseIndex<3; caseIndex++)
    for(int caseIndex=0; caseIndex<6; caseIndex++)
    {
        for(strategy_idx=0; strategy_idx<STRATEGY_CNT; strategy_idx++)
        //for(strategy_idx=0; strategy_idx<1; strategy_idx++)
        {
            //Woodcarving(); //simple example of .mod & .sol method
            config_setting(caseIndex);
            data_parser(caseIndex);
            config_setting_with_shift(shift->shiftCategory, caseIndex);
            glp_check_constraint_enable();
            singleResult_init(caseIndex);
            singleResult_countDown();
#ifdef GLPSOL
            gmpl_method(caseIndex, filesol[caseIndex]);
#else
            gen_glp_problem();
#endif
            singleResult_saveTime();
            gpointer_delete();
            config_delete();
        }
    }
    memoryRelease(backup_row_constraint_table);
#ifdef GLPSOL
    for(int caseIndex=0; caseIndex<ProbCace_NUMBER; caseIndex++)
        show_gmpl_sol(filesol[caseIndex]);
#endif
    singleResult_printHRStatus();
    allResult_free();
    gettimeofday(&t2, NULL);

    // compute and print the elapsed time in millisec
    elapsedTime = (t2.tv_sec - t1.tv_sec);// * 1000.0;      // sec to ms
    //elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
    printf("%f s.\n", elapsedTime);
    return 0;
}
