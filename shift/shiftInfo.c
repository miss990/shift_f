#include "shiftInfo.h"
#include "memory.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


Shift_t* createEmptyShift(void)
{
    Shift_t* shift = (Shift_t *)malloc(sizeof(Shift_t));//3

    memset(&shift->minRequiredStaff, 0, sizeof(shift->minRequiredStaff));
    memset(&shift->minRequiredLeader, 0, sizeof(shift->minRequiredLeader));
    memset(&shift->minRequiredCharge, 0, sizeof(shift->minRequiredCharge));
    memset(&shift->weekend, 0, sizeof(shift->weekend));
    memset(&shift->total_shift, 0, sizeof(shift->total_shift));
    memset(&shift->total_nurse_in_one_shift, 0, sizeof(shift->total_nurse_in_one_shift));
    shift->shiftCategory = 0;
    shift->MinOffDutyDay = 0; //當月應休時數
    return shift;
}

void setNameOfMonthAndNumberOfDaysInShift(Shift_t* shift, char *month, int totalDaysInMonth)
{
    shift->month = month; 
    shift->totalDaysInMonth = totalDaysInMonth;
}

void printShift(Shift_t* shift)
{
    printf("There are %d days in %s, . \n", shift->totalDaysInMonth, shift->month);
}

void printShiftRecord(Shift_t* shift)
{
    printf("==== Shift Info ====\n");
    printf("There are %d days in %s.\n", shift->totalDaysInMonth, shift->month);
    printf("Shift: %d\n", shift->shiftCategory);
    printf("Total Weekends: %d\n", shift->weekend[WEEKEND_CNT-1]);
    printf("Each day minimal nurse number :\n");
    printMinRequiredStaffInShifts(shift);
    printf("\n");
}

void printMinRequiredStaffInShifts(Shift_t* shift)
{
    int total_shift = get_config_total_shift();
    
    if(total_shift == 3)
    {
        for(int cnt = SHIFT_DAY; cnt < SHIFT_NUMBER; cnt++)
        {
            printMinRequiredStaffInShift(shift, cnt);
        }
    }
    else if(total_shift == 1)
    {
        printMinRequiredStaffInShift(shift, shift->shiftCategory);
    }
    
}

void printMinRequiredStaffInShift(Shift_t* shift, ShiftCategory_t shift_c)
{
    int total_workday = get_config_total_workday();
    printf("    %d: [", shift_c);
    for (int day = 0; day < total_workday; day++)
    {
        if (day != total_workday - 1)
        {
            printf("%d, ", shift->minRequiredStaff[shift_c][day]);
            
            continue;
        }
        printf("%d]\n", shift->minRequiredStaff[shift_c][day]);
    }
}
