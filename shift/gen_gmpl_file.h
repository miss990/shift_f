//
//  gen_gmpl_file.h
//  shift
//
//  Created by 張柏勛 on 2019/6/4.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#ifndef gen_gmpl_file_h
#define gen_gmpl_file_h

#include <stdio.h>
#include "share_var.h"


void gen_gmpl_file(void);
void gmpl_var(void);
void gmpl_objective(void);
void gmpl_constraint(void);
void gmpl_unique_status(int index);
void gmpl_staff_demand(int index);
void gmpl_leader_demand(int index);
void gmpl_charge_demand(int index);
void gmpl_position_limit(int index);
void gmpl_shift_limit(int index);
void gmpl_leave_quota(int index);
void gmpl_long_term_leave(int index);
void gmpl_green_line_leave(int index);
void gmpl_stand_by_demand(int index);
void gmpl_stand_by_leveling(int index);
void gmpl_stand_by_limit(int index);
void gmpl_mid_shift_demand(int index);
void gmpl_mid_shift_leveling(int index);
void gmpl_mid_shift_limit(int index);
void gmpl_continuous_on_duty(int index);
void gmpl_off_duty_separate(int index);
void gmpl_min_off_duty_day(int index);
void gmpl_weekend_leveling(int index);
void gmpl_shift_to_shift(int index);
void gmpl_night_shift_15day(int index);

extern FILE *fout;



#endif /* gen_gmpl_file_h */
