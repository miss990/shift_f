//
//  gen_gmpl_file.c
//  shift
//
//  Created by 張柏勛 on 2019/6/4.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#include "gen_gmpl_file.h"
#include "gen_glp_problem.h"

extern tCONSTRAINT row_constraint_table[ROW_NUMBER];

void (*gmpl_constraint_table[ROW_NUMBER])(int) =
{
    gmpl_unique_status,
    gmpl_staff_demand,
    gmpl_leader_demand,
    gmpl_charge_demand,
    gmpl_position_limit,
    gmpl_shift_limit,
    gmpl_leave_quota,
    gmpl_long_term_leave,
    gmpl_green_line_leave,
    gmpl_stand_by_demand,
    gmpl_stand_by_leveling,
    gmpl_stand_by_limit,
    gmpl_mid_shift_demand,
    gmpl_mid_shift_leveling,
    gmpl_mid_shift_limit,
    gmpl_continuous_on_duty,
    gmpl_off_duty_separate,
    gmpl_min_off_duty_day,
    gmpl_weekend_leveling,
    gmpl_shift_to_shift,
    gmpl_night_shift_15day
};

void gen_gmpl_file(void)
{
    gmpl_var();
    cal_obj_coef_modified_parameter();
    gmpl_objective();
    gmpl_constraint();
    
    //After shift.mod been generated,
    //execute "glpsol -m shift.mod --output shift.sol" at termenal
    //then check the solution in shift.sol
}

void gmpl_var(void)
{
    int total_nurse = get_config_total_nurse();
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    int last_6workday_last_month = get_config_last_6workday_last_month();
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        for(int DayIncludeLastMonth = 0; DayIncludeLastMonth < last_month_p_total_workday; DayIncludeLastMonth++)
        {
            for(int HR_status = 0; HR_status < HRSTATUS_NUMBER; HR_status++)
            {
                char var_c[80] = "";
                var_num2char(nurse, DayIncludeLastMonth, var_c, HR_status, false);
                if(DayIncludeLastMonth < last_6workday_last_month)//results of last 6 days of last month
                {
                    int result = staffs[nurse].LMShiftList[DayIncludeLastMonth];
                    int bnd = (result == HR_status) ? 1 : 0;
                    debuglog("var %s=%d; ", var_c, bnd);
                }
                else
                {
                    debuglog("var %s, binary; ", var_c);
                }
            }
        }
        debuglog("\n");
    }
    debuglog("\n");
}

void gmpl_objective(void)
{
    int total_nurse = get_config_total_nurse();
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    int last_6workday_last_month = get_config_last_6workday_last_month();
    int sum_cnt = (total_nurse * last_month_p_total_workday)-1;
    int day;
    double score, modifiedScoreParameter, redEnhancementCoeff, redEnhancementCoeff_nurse;
    debuglog("%s z: ", OBJECTIVE_DIRECTION);
    ShiftCategory_t shift_c;
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        shift_c                   = staffs[nurse].default_shift;
        modifiedScoreParameter    = staffs[nurse].modifiedScoreParameter[MSP_TOTAL];
        redEnhancementCoeff_nurse = get_red_enhancement_coefficient(staffs[nurse].askDayRedLeave);
        for(int DayIncludeLastMonth = 0; DayIncludeLastMonth < last_month_p_total_workday; DayIncludeLastMonth++)
        {
            int dayThisMonth = get_day_minus_last_6workday_last_month(DayIncludeLastMonth);
            HRStatus_t leave = staffs[nurse].AskLeaveList[dayThisMonth];
            
            redEnhancementCoeff = get_red_enhancement_coeff_on_date(redEnhancementCoeff_nurse, leave);
            
            for(int HR_status = 0; HR_status < HRSTATUS_NUMBER; HR_status++)
            {
                char var_c[80] = "";
                var_num2char(nurse, DayIncludeLastMonth, var_c, HR_status, false);
                
                if(HR_status)  debuglog(" + ");
                if(DayIncludeLastMonth < last_6workday_last_month)
                {
                    //debuglog("(%d * %s)", feeling(staffs[nurse].LMShiftList[DayIncludeLastMonth], HR_status), var_c);
                    debuglog("(%d * %s)", 0, var_c);
                }
                else
                {
                    day = get_day_minus_last_6workday_last_month(DayIncludeLastMonth);
                    score = feeling(staffs[nurse].AskLeaveList[day], HR_status, shift_c)
                        * modifiedScoreParameter * redEnhancementCoeff;
                    debuglog("(%f * %s)", score, var_c);
                }
            }
            
            if(sum_cnt--) debuglog(" + ");
            else debuglog(";");
        }
        debuglog("\n");
    }
    debuglog("\n");
}

void gmpl_var_summation(int nurse, int DayIncludeLastMonth, HRStatus_t HR_status, char* op, int *cnt)
{
    char var_c[80] = "";
    var_num2char(nurse, DayIncludeLastMonth, var_c, HR_status, false);
    
    if(*cnt) debuglog(" %s ", op);
    debuglog("%s", var_c);
    (*cnt)++;
}

void gmpl_unique_status(int index)
{
    int DayIncludeLastMonth, unique_cnt;
    int total_workday = get_config_total_workday();
    int on_duty_cnt   = get_config_on_duty_cnt();
    int stand_by_cnt  = get_config_stand_by_cnt();
    int off_duty_cnt  = get_config_off_duty_cnt();
    int mid_shift_cnt = get_config_mid_shift_cnt();
    int status_cnt    = on_duty_cnt + stand_by_cnt + off_duty_cnt + mid_shift_cnt;
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(status_cnt, sizeof(HRStatus_t));
        
        get_on_duty_HRStatus(HRStatus_list, shift_c, false);
        get_off_duty_HRStatus(HRStatus_list + on_duty_cnt, shift_c, true);
        get_mid_shift_HRStatus(HRStatus_list + (status_cnt-mid_shift_cnt));
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            for(int day = 0; day < total_workday; day++)
            {
                unique_cnt = 0;
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                debuglog("s.t. N%dD%d_unique_status : ", nurse_c, DayIncludeLastMonth);
                
                for(int cnt = 0; cnt < status_cnt; cnt++)
                {
                    HRStatus_t HR_status = HRStatus_list[cnt];
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, HR_status, "+", &unique_cnt);
                }
                debuglog(" = 1;\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
    debuglog("\n");
}

void gmpl_staff_demand(int index)
{
    int DayIncludeLastMonth, staff_cnt;
    int total_workday = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        for(int day = 0; day < total_workday; day++)
        {
            staff_cnt = 0;
            DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
            debuglog("s.t. S%dD%d_staff : ", shift_c, DayIncludeLastMonth);
            
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                if(is_nurse_grade_meet_position(nurse_c, get_staff_on_duty(shift_c), shift_c))
                {
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, get_staff_on_duty(shift_c), "+", &staff_cnt);
                }
            }
            debuglog(" = %d;\n", shift->minRequiredStaff[shift_c][day]);
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_leader_demand(int index)
{
    int DayIncludeLastMonth, leader_cnt;
    int total_workday          = get_config_total_workday();
    ShiftCategory_t* min_leader_per_workday = get_config_min_leader_per_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(min_leader_per_workday[shift_c] > 0)
        {
            for(int day = 0; day < total_workday; day++)
            {
                leader_cnt = 0;
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                debuglog("s.t. S%dD%d_leader : ", shift_c, DayIncludeLastMonth);
            
                for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
                {
                    if(is_nurse_grade_meet_position(nurse_c, get_leader_on_duty(shift_c), shift_c))
                    {
                        gmpl_var_summation(nurse_c, DayIncludeLastMonth, get_leader_on_duty(shift_c), "+", &leader_cnt);
                    }
                }
                debuglog(" = %d;\n", min_leader_per_workday[shift_c]);
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_charge_demand(int index)
{
    int DayIncludeLastMonth, charge_cnt;
    int total_workday          = get_config_total_workday();
    ShiftCategory_t* min_charge_per_workday = get_config_min_charge_per_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(min_charge_per_workday[shift_c] > 0)
        {
            for(int day = 0; day < total_workday; day++)
            {
                charge_cnt = 0;
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                debuglog("s.t. S%dD%d_charge : ", shift_c, DayIncludeLastMonth);
                for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
                {
                    if(is_nurse_grade_meet_position(nurse_c, get_charge_on_duty(shift_c), shift_c))
                    {
                        gmpl_var_summation(nurse_c, DayIncludeLastMonth, get_charge_on_duty(shift_c), "+", &charge_cnt);
                    }
                }
                debuglog(" = %d;\n", min_charge_per_workday[shift_c]);
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_position_limit(int index)
{
    int DayIncludeLastMonth, not_meet_cnt;
    int total_workday = get_config_total_workday();
    int on_duty_cnt   = get_config_on_duty_cnt() + get_config_stand_by_cnt();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
        get_on_duty_HRStatus(HRStatus_list, shift_c, true);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            if(check_grade_not_meet_any_position(staffs[nurse_c].job_grade, shift_c))
            {
                debuglog("s.t. S%dN%d_position : ", shift_c, nurse_c);
                not_meet_cnt = 0;
                for(int cnt = 0; cnt < on_duty_cnt; cnt++)
                {
                    HRStatus_t on = HRStatus_list[cnt];
                    if(is_nurse_grade_meet_position(nurse_c, on, shift_c))
                        continue;
                    for(int day = 0; day < total_workday; day++)
                    {
                        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                        gmpl_var_summation(nurse_c, DayIncludeLastMonth, on, "+", &not_meet_cnt);
                    }
                }
                debuglog(" = 0;\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
    debuglog("\n");
}

void gmpl_shift_limit(int index)
{
    int DayIncludeLastMonth, other_cnt;
    int total_nurse       = get_config_total_nurse();
    int total_workday     = get_config_total_workday();
    int other_on_duty_cnt = get_config_on_duty_cnt() + get_config_stand_by_cnt();
    int total_shift       = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    ShiftCategory_t def_shift;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        def_shift = staffs[nurse].default_shift;
        debuglog("s.t. S%dN%d_shift : ", def_shift, nurse);
        other_cnt = 0;
        for(shift_c = shift_s; shift_c < shift_e; shift_c++)
        {
            if(shift_c == def_shift) continue;
            
            HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(other_on_duty_cnt, sizeof(HRStatus_t));
            get_on_duty_HRStatus(HRStatus_list, shift_c, true);
            
            for(int day = 0; day < total_workday; day++)
            {
                for(int cnt = 0; cnt < other_on_duty_cnt; cnt++)
                {
                    HRStatus_t other = HRStatus_list[cnt];
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    gmpl_var_summation(nurse, DayIncludeLastMonth, other, "+", &other_cnt);
                }
            }
            free(HRStatus_list);
        }
        debuglog(" = 0;\n");
    }
    debuglog("\n");
}

void gmpl_leave_quota(int index)
{
    int DayIncludeLastMonth, leave_cnt;
    int total_workday = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt  = get_config_off_duty_cnt();
        bool stand_by = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            leave_cnt = 0;
            debuglog("s.t. S%dN%d_leave_quota : ", shift_c, nurse_c);
            for(int day = 0; day < total_workday; day++)
            {
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                
                for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                {
                    HRStatus_t off = HRStatus_list[cnt];
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, off, "+", &leave_cnt);
                }
            }
            debuglog(" <= %d;\n", getHour2FloorDay(staffs[nurse_c].leaveQuota_Hour));
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
    debuglog("\n");
}

void gmpl_long_term_leave(int index)
{
    int DayIncludeLastMonth, long_cnt;
    int total_nurse   = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        debuglog("s.t. N%d_long_term_leave : ", nurse);
        long_cnt = 0;
        for(int day = 0; day < total_workday; day++)
        {
            DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
            gmpl_var_summation(nurse, DayIncludeLastMonth, HRSTATUS_LONG_OFFDUTY, "+", &long_cnt);
        }
        if(staffs[nurse].longtermleaveask > 0)
            debuglog(" <= %d;\n", staffs[nurse].longtermleaveask);
        else
            debuglog(" = 0;\n");
    }
    debuglog("\n");
}

void gmpl_green_line_leave(int index)
{
    int DayIncludeLastMonth, green_cnt;
    int total_nurse   = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        green_cnt = 0;
        debuglog("s.t. N%d_green_line_leave : ", nurse);
        for(int day = 0; day < total_workday; day++)
        {
            DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
            gmpl_var_summation(nurse, DayIncludeLastMonth, HRSTATUS_GREEN_OFFDUTY, "+", &green_cnt);
        }
        if(staffs[nurse].greenleaveask > 0)
            debuglog(" <= %d;\n", staffs[nurse].greenleaveask);
        else
            debuglog(" = 0;\n");
    }
    debuglog("\n");
}

void gmpl_stand_by_demand(int index)
{
    int DayIncludeLastMonth, standby_cnt;
    int total_nurse              = get_config_total_nurse();
    int total_workday            = get_config_total_workday();
    int min_stand_by_per_workday = 1;
    int shift_c;
    int nurse_c;
    
    for(int day = 0; day < total_workday; day++)
    {
        standby_cnt = 0;
        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
        debuglog("s.t. D%d_stand_by_demand : ", DayIncludeLastMonth);
        for(nurse_c = 0; nurse_c < total_nurse; nurse_c++)
        {
            shift_c = staffs[nurse_c].default_shift;
            if(is_in_Stand_by_pool(shift_c))
            {
                if(is_nurse_grade_meet_position(nurse_c, get_stand_by(shift_c), shift_c))
                {
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, get_stand_by(shift_c), "+", &standby_cnt);
                }
            }
        }
        debuglog(" = %d;\n", min_stand_by_per_workday);
    }
    debuglog("\n");
}

void gmpl_stand_by_leveling(int index)
{
    int DayIncludeLastMonth, standby_cnt;
    int total_workday  = get_config_total_workday();
    int* shift_nurse   = get_config_shift_nurse();
    int total_shift    = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    int stand_by_bound[2] = {0};
    get_bound_per_poolnurse(HRSTATUS_STANDBY_DAY, stand_by_bound);

    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(is_in_Stand_by_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                standby_cnt = 0;
                if(is_nurse_grade_meet_position(nurse_c, get_stand_by(shift_c), shift_c))
                    debuglog("s.t. S%dN%d_stand_by_leveling : %d <= ", shift_c, nurse_c, stand_by_bound[0]);
                else
                    debuglog("s.t. S%dN%d_stand_by_leveling : ", shift_c, nurse_c);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, get_stand_by(shift_c), "+", &standby_cnt);
                }
                
                if(is_nurse_grade_meet_position(nurse_c, get_stand_by(shift_c), shift_c))
                    debuglog(" <= %d;\n", stand_by_bound[1]);
                else
                    debuglog(" = 0;\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_stand_by_limit(int index)
{
    int DayIncludeLastMonth, standby_cnt;
    int total_workday  = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(!is_in_Stand_by_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                standby_cnt = 0;
                debuglog("s.t. S%dN%d_stand_by_limit : ", shift_c, nurse_c);
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, get_stand_by(shift_c), "+", &standby_cnt);
                }
                debuglog(" = 0;\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_mid_shift_demand(int index)
{
    //s.t. mid_shift_demand    TOTAL_WORKDAY
    int DayIncludeLastMonth, midshift_cnt;
    int total_nurse              = get_config_total_nurse();
    int total_workday            = get_config_total_workday();
    int min_mid_shift_per_workday = 1;
    int first_day                = get_config_first_day_is();
    int shift_c;
    int nurse_c;
    
    for(int day = 0; day < total_workday; day++)
    {
        midshift_cnt = 0;
        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
        debuglog("s.t. D%d_mid_shift_demand : ", DayIncludeLastMonth);
        
        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
        for(nurse_c = 0; nurse_c < total_nurse; nurse_c++)
        {
            shift_c = staffs[nurse_c].default_shift;
            if(is_in_mid_shift_pool(shift_c))
            {
                if(is_nurse_grade_meet_position(nurse_c, HRSTATUS_MID_ONDUTY, shift_c))
                {
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, HRSTATUS_MID_ONDUTY, "+", &midshift_cnt);
                }
            }
        }
        if(is_weekend(day, first_day))
            debuglog(" = %d;\n", 0);
        else
            debuglog(" = %d;\n", min_mid_shift_per_workday);
    }
    debuglog("\n");
}

void gmpl_mid_shift_leveling(int index)
{
    int DayIncludeLastMonth, midshift_cnt;
    int total_workday  = get_config_total_workday();
    int* shift_nurse   = get_config_shift_nurse();
    int total_shift    = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    int mid_shift_bound[2] = {0};
    get_bound_per_poolnurse(HRSTATUS_MID_ONDUTY, mid_shift_bound);
    
    mid_shift_bound[0] = 1;//Workaround 5
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(is_in_mid_shift_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                midshift_cnt = 0;
                if(is_nurse_grade_meet_position(nurse_c, HRSTATUS_MID_ONDUTY, shift_c))
                    debuglog("s.t. S%dN%d_mid_shift_leveling : %d <= ", shift_c, nurse_c, mid_shift_bound[0]);
                else
                    debuglog("s.t. S%dN%d_mid_shift_leveling : ", shift_c, nurse_c);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, HRSTATUS_MID_ONDUTY, "+", &midshift_cnt);
                }
                if(is_nurse_grade_meet_position(nurse_c, HRSTATUS_MID_ONDUTY, shift_c))
                    debuglog(" <= %d;\n", mid_shift_bound[1]);
                else
                    debuglog(" = 0;\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_mid_shift_limit(int index)
{
    int DayIncludeLastMonth, midshift_cnt;
    int total_workday  = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(!is_in_mid_shift_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                midshift_cnt = 0;
                debuglog("s.t. S%dN%d_mid_shift_limit : ", shift_c, nurse_c);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, HRSTATUS_MID_ONDUTY, "+", &midshift_cnt);
                }
                debuglog(" = 0;\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_continuous_on_duty(int index)
{
    int day_range, cont_cnt;
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    int* shift_nurse               = get_config_shift_nurse();
    int total_shift                = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int on_duty_cnt            = get_config_on_duty_cnt();
        bool stand_by              = is_Stand_By_required();
        bool mid_shift             = is_mid_shift_required();
        if(stand_by)  on_duty_cnt += get_config_stand_by_cnt();
        int mid_cnt                = get_config_mid_shift_cnt();
        if(mid_shift) on_duty_cnt += mid_cnt;
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
        get_on_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        if(mid_shift) get_mid_shift_HRStatus(HRStatus_list + (on_duty_cnt-mid_cnt));
        int cont_on_duty_limit = get_cont_workday_limit(shift_c);
        int sets_to_consider   = (last_month_p_total_workday - cont_on_duty_limit);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            for(int day = 0; day < sets_to_consider; day++)
            {
                cont_cnt = 0;
                debuglog("s.t. S%dN%dD%d_cont_on_duty : ", shift_c, nurse_c, day);
                
                //Only allow up to [cont_on_duty_limit] days of on duty in any [cont_on_duty_limit + 1] days
                day_range = (day + cont_on_duty_limit + 1);
                for(int range_cnt = day; range_cnt < day_range; range_cnt++)
                {
                    for(int cnt = 0; cnt < on_duty_cnt; cnt++)
                    {
                        HRStatus_t on = HRStatus_list[cnt];
                        gmpl_var_summation(nurse_c, range_cnt, on, "+", &cont_cnt);
                    }
                }
                debuglog(" <= %d;\n", cont_on_duty_limit);
            }
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
    debuglog("\n");
}

void gmpl_off_duty_separate(int index)
{
    int DayIncludeLastMonth, day_range, separate_cnt;
    int total_workday       = get_config_total_workday();
    int sets_to_consider    = (total_workday-2);
    int* shift_nurse               = get_config_shift_nurse();
    int total_shift                = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt        = get_config_on_duty_cnt();
        bool stand_by           = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            for(int day = 0; day < sets_to_consider; day++)
            {
                separate_cnt = 0;
                debuglog("s.t. S%dN%dD%d_off_duty_separate : -1 <= ", shift_c, nurse_c, day);
                day_range = day + 3;
                for(int range_cnt = day; range_cnt < day_range; range_cnt++)
                {
                    for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                    {
                        HRStatus_t off = HRStatus_list[cnt];
                        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(range_cnt);
                        if(range_cnt == day+1) gmpl_var_summation(nurse_c, DayIncludeLastMonth, off, "-", &separate_cnt);
                        else                   gmpl_var_summation(nurse_c, DayIncludeLastMonth, off, "+", &separate_cnt);
                    }
                }
                debuglog(" <= 1;\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
    debuglog("\n");
}

void gmpl_min_off_duty_day(int index)
{
    int DayIncludeLastMonth, off_cnt;
    int total_workday = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    int MaxOffDutyDay = get_config_max_off_duty_per_staff();
    int MinOffDutyDay = get_config_min_off_duty_per_staff();
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
        //MinOffDutyDay = shift->MinOffDutyDay;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt           = get_config_off_duty_cnt();
        bool stand_by              = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            debuglog("s.t. S%dN%d_min_off_duty_day : ", shift_c, nurse_c);
            off_cnt = 0;
            debuglog(" %d <= ", MinOffDutyDay);
            for(int day = 0; day < total_workday; day++)
            {
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                {
                    HRStatus_t off = HRStatus_list[cnt];
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, off, "+", &off_cnt);
                }
            }
            debuglog(" <= %d;\n", MaxOffDutyDay);
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
    debuglog("\n");
}

void gmpl_weekend_leveling(int index)
{
    int DayIncludeLastMonth, day, weekend_cnt;
    int total_weekend  = shift->weekend[WEEKEND_CNT-1];
    int weekend_lb     = get_weekend_lower_bound();
    int weekend_ub     = get_weekend_upper_bound();
    int* shift_nurse   = get_config_shift_nurse();
    int total_shift    = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt           = get_config_off_duty_cnt();
        bool stand_by              = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            if(nurse_c == 0) continue;//nurse #0 always day off on weekend
            debuglog("s.t. S%dN%d_weekend_leveling : ", shift_c, nurse_c);
            weekend_cnt = 0;
            debuglog(" %d <= ", weekend_lb);
            for(int weekend_c = 0; weekend_c < total_weekend; weekend_c++)
            {
                day = shift->weekend[weekend_c];
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                {
                    HRStatus_t off = HRStatus_list[cnt];
                    gmpl_var_summation(nurse_c, DayIncludeLastMonth, off, "+", &weekend_cnt);
                }
            }
            debuglog(" <= %d;\n", weekend_ub);
        }
        free(HRStatus_list);
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_shift_to_shift(int index)
{
    int lastM_shiftcategory, lastM_lastD, thisM_shiftcategory, sts, shift_cnt;
    int total_nurse              = get_config_total_nurse();
    int last_6workday_last_month = get_config_last_6workday_last_month();
    int day_start                = last_6workday_last_month-1;
    int day_end                  = last_6workday_last_month;
    int on_duty_cnt              = get_config_on_duty_cnt();
    ShiftCategory_t shift_c      = shift->shiftCategory;
    bool stand_by                = is_Stand_By_required();
    
    if(stand_by) on_duty_cnt    += get_config_stand_by_cnt();
    
    HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
    get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        //上月最後一天與本月第一天，依據班別組合，最多可以是兩天都上班
        lastM_shiftcategory = staffs[nurse].lastmonth_shift;
        lastM_lastD         = staffs[nurse].LMShiftList[last_6workday_last_month-1];
        thisM_shiftcategory = shift->shiftCategory;
        sts                 = is_1st_day_to_work(lastM_shiftcategory, lastM_lastD, thisM_shiftcategory);
        debuglog("s.t. N%d_shift_to_shift : ", nurse);
        shift_cnt = 0;
        
        //上月最後一天與本月第一天
        for(int DayIncludeLastMonth = day_start; DayIncludeLastMonth <= day_end; DayIncludeLastMonth++)
        {
            for(int cnt = 0; cnt < on_duty_cnt; cnt++)
            {
                HRStatus_t on = HRStatus_list[cnt];
                gmpl_var_summation(nurse, DayIncludeLastMonth, on, "+", &shift_cnt);
            }
        }
        debuglog(" <= %d;\n", sts);
    }
    free(HRStatus_list);
    debuglog("\n");
}

void gmpl_night_shift_15day(int index)
{
    int DayIncludeLastMonth, night_cnt;
    int total_workday                = get_config_total_workday();
    int min_on_duty_days_night_shift = get_config_min_on_duty_days_night_shift();
    int on_duty_cnt                  = get_config_on_duty_cnt();
    int* shift_nurse                 = get_config_shift_nurse();
    int total_shift                  = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(shift_c != SHIFT_DAY)
        {
            HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
            get_on_duty_HRStatus(HRStatus_list, shift_c, false);
            
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                debuglog("s.t. S%dN%d_night_shift : ", shift_c, nurse_c);
                night_cnt = 0;
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    
                    for(int cnt = 0; cnt < on_duty_cnt; cnt++)
                    {
                        HRStatus_t on = HRStatus_list[cnt];
                        gmpl_var_summation(nurse_c, DayIncludeLastMonth, on, "+", &night_cnt);
                    }
                }
                debuglog(" >= %d;\n", min_on_duty_days_night_shift);
            }
            free(HRStatus_list);
        }
        nurse_s += shift_nurse[shift_c];
    }
    debuglog("\n");
}

void gmpl_constraint(void)
{
    for(int cnt=0; cnt < ROW_NUMBER; cnt++)
    {
        if(row_constraint_table[cnt].enable)
        {
            printf("Enable %s\n", row_constraint_table[cnt].name);
            gmpl_constraint_table[cnt](cnt);
        }
        else
            printf("Disable %s\n", row_constraint_table[cnt].name);
    }
    debuglog("end;");
}
