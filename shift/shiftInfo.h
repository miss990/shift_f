#ifndef shiftInfo_h
#define shiftInfo_h

#include "config.h"
#include <stdbool.h>

typedef struct
{
    const char *month;
    int totalDaysInMonth;  // couldn't use map / dictionary structure -> it might be 28 / 29 days in Frb
    int minRequiredStaff[SHIFT_NUMBER][WORKDAY_ARRAY_SUPPORT]; //in this shift of this month, minum staffs are required each day
    int minRequiredLeader[SHIFT_NUMBER][WORKDAY_ARRAY_SUPPORT];
    int minRequiredCharge[SHIFT_NUMBER][WORKDAY_ARRAY_SUPPORT];
    int weekend[WEEKEND_CNT];//There are 10 weekend days in a months at most, index 10 keep the total days in this month
    int shiftCategory;
    int total_shift[MAX_TOTAL_SHIFT];
    int total_nurse_in_one_shift[MAX_TOTAL_SHIFT];
    int MinOffDutyDay; //當月應休天數
}Shift_t;

Shift_t* createEmptyShift(void);

void setNameOfMonthAndNumberOfDaysInShift(Shift_t* shift, char *month, int totalDaysInMonth);
void printShift(Shift_t* shift);
void printShiftRecord(Shift_t* shift);
void printMinRequiredStaffInShifts(Shift_t* shift);
void printMinRequiredStaffInShift(Shift_t* shift, ShiftCategory_t shift_c);
#endif
