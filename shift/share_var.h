//
//  share_var.h
//  shift
//
//  Created by 張柏勛 on 2019/6/5.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#ifndef share_var_h
#define share_var_h

#include "staffInfo.h"
#include "shiftInfo.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include "glpk.h"

extern Staff_t *staffs;
extern Shift_t *shift;
extern int score_table[SCORE_TABLE_ASKING][SCORE_TABLE_VARIABLE];
//#define PRINT
#ifdef PRINT
    #define debuglog( ...)  printf(__VA_ARGS__)
#else
    #define debuglog( ...)  fprintf(fout, __VA_ARGS__)
#endif

typedef enum{
    SOCRE_UNREASONABLE   = -200,
    SCORE_Q_DISAPPOINTED = -40,
    SCORE_DISAPPOINTED   = -15,
    SCORE_SAD            = -3,
    SCORE_UNCONCERNED    = 0,
    SCORE_HAPPINESS      = 5,
    SCORE_SATISFIED      = 10,
    SCORE_Q_SATISFIED    = 30
}Score_t;

static inline int is_nurse_grade_meet_position(int nurse, HRStatus_t HR_status, ShiftCategory_t shift_c);
static inline JobPosition_t classify_job_position(HRStatus_t HR_status);
static inline bool read_grade_fit_position_value(bool* grade_fit_position, JobGrade_t grade, JobPosition_t position,
                                                 ShiftCategory_t shift_c);

static inline void st_num2char(int st_cnt, char* st_char, char nurse_c[80])
{
    char cnt[] = "9999";
    strcpy(nurse_c, st_char);
    sprintf(cnt, "%d", st_cnt);
    //printf("nurse_c %s, cnt %s\n", nurse_c, cnt);
    strcat(nurse_c, cnt);
}

static inline void var_num2char(int nurse_cnt, int day_cnt, char var_c[80], HRStatus_t HR_status, bool realname)
{
    int last_6workday_last_month = get_config_last_6workday_last_month();
    char cnt[] = "9999";
    
    if(realname)  strcpy(var_c, staffs[nurse_cnt].name);
    else
    {
        strcpy(var_c, "N");
        sprintf(cnt, "%d", nurse_cnt);
        strcat(var_c, cnt);
    }
    
    if(day_cnt < last_6workday_last_month)  strcat(var_c, "LMD");
    else                                    strcat(var_c, "D");
    
    if(realname)  day_cnt -= last_6workday_last_month;
    sprintf(cnt, "%d", day_cnt);
    strcat(var_c, cnt);
    
    if     (HR_status == HRSTATUS_CHARGE_ONDUTY_DAY)        strcat(var_c, "DCON");
    else if(HR_status == HRSTATUS_LEADER_ONDUTY_DAY)        strcat(var_c, "DLON");
    else if(HR_status == HRSTATUS_STAFF_ONDUTY_DAY)         strcat(var_c, "DSON");
    else if(HR_status == HRSTATUS_STANDBY_DAY)              strcat(var_c, "DSTB");
    
    else if(HR_status == HRSTATUS_CHARGE_ONDUTY_NIGHT)      strcat(var_c, "NCON");
    else if(HR_status == HRSTATUS_LEADER_ONDUTY_NIGHT)      strcat(var_c, "NLON");
    else if(HR_status == HRSTATUS_STAFF_ONDUTY_NIGHT)       strcat(var_c, "NSON");
    else if(HR_status == HRSTATUS_STANDBY_NIGHT)            strcat(var_c, "NSTB");
    
    else if(HR_status == HRSTATUS_CHARGE_ONDUTY_GRAVEYARD)  strcat(var_c, "GCON");
    else if(HR_status == HRSTATUS_LEADER_ONDUTY_GRAVEYARD)  strcat(var_c, "GLON");
    else if(HR_status == HRSTATUS_STAFF_ONDUTY_GRAVEYARD)   strcat(var_c, "GSON");
    else if(HR_status == HRSTATUS_STANDBY_GRAVEYARD)        strcat(var_c, "GSTB");
    
    else if(HR_status == HRSTATUS_RED_OFFDUTY)              strcat(var_c, "ROF");
    else if(HR_status == HRSTATUS_RED_ENHANCE_OFFDUTY)      strcat(var_c, "REOF");
    else if(HR_status == HRSTATUS_GREEN_OFFDUTY)            strcat(var_c, "GOF");
    else if(HR_status == HRSTATUS_LONG_OFFDUTY)             strcat(var_c, "LOF");
    
    else if(HR_status == HRSTATUS_MID_ONDUTY)               strcat(var_c, "MON");
    else glp_assert(false);
    //printf("var_c %s\n", var_c);
}

static inline int col_map(int nurse, int day, int colstatus)
{
/*
 [                                                                                nurse 0]
 [                                          LM day 0]...[LM day 5] [   day 0]...[  day 30]
 [C_OnDuty] [L_OnDuty] [S_OnDuty] [OffDuty] [StandBy]...[   ...  ] [   ...  ]...[   ...  ]
 [       1] [       2] [       3] [      4] [      5]...[ 26 ~ 30] [ 31 ~ 35]...[181 ~185]
 
 [                                                                                nurse 1]
 [                                          LM day 0]...[LM day 5] [   day 0]...[  day 30]
 [C_OnDuty] [L_OnDuty] [S_OnDuty] [OffDuty] [StandBy]...[   ...  ] [   ...  ]...[   ...  ]
 [     186] [     187] [     188] [    189] [    190]...[211 ~215] [216 ~220]...[366 ~370]
*/
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    return (((nurse * last_month_p_total_workday * HRSTATUS_NUMBER) + (day * HRSTATUS_NUMBER) + colstatus) + 1);
}

static inline int get_cont_workday_limit(int shiftCategory)
{
    if(shiftCategory == SHIFT_DAY)
        return get_config_cont_on_duty_limit_day_shift();
    else if(shiftCategory == SHIFT_NIGHT || shiftCategory == SHIFT_GRAVEYARD)
        return get_config_cont_on_duty_limit_night_shift();
    else
    {
        glp_assert(false);
        return 0;
    }
}

static inline int get_day_plus_last_6workday_last_month(int day)
{
    return (get_config_last_6workday_last_month() + day);
}

static inline int get_day_minus_last_6workday_last_month(int day)
{
    return (day - get_config_last_6workday_last_month());
}

static inline int get_unique_status_cnt(void)
{
    int total_nurse = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    return (total_nurse * total_workday);
}

static inline int get_staff_demand_cnt(void)
{
    int total_shift = get_config_total_shift();
    return (total_shift * get_config_total_workday());
}

static inline int get_leader_demand_cnt(void)
{
    //int total_shift = get_config_total_shift();
    //return (total_shift * get_config_total_workday());
    
    int total_shift  = SHIFT_NUMBER;
    ShiftCategory_t* min_leader_per_workday = get_config_min_leader_per_workday();
    int st_cnt       = 0;
    for(int shift_c = 0; shift_c < total_shift; shift_c++)
    {
        if(min_leader_per_workday[shift_c] > 0)
            st_cnt++;
    }
    return (st_cnt * get_config_total_workday());
}

static inline int get_charge_demand_cnt(void)
{
    //int total_shift = get_config_total_shift();
    //return (total_shift * get_config_total_workday());
    int total_shift  = SHIFT_NUMBER;
    ShiftCategory_t* min_charge_per_workday = get_config_min_charge_per_workday();
    int st_cnt       = 0;
    for(int shift_c = 0; shift_c < total_shift; shift_c++)
    {
        if(min_charge_per_workday[shift_c] > 0)
            st_cnt++;
    }
    return (st_cnt * get_config_total_workday());
}

static inline int get_position_limit_cnt(void)
{
    return (get_config_total_nurse());
}

static inline int get_shift_limit_cnt(void)
{
    return (get_config_total_nurse());
}

static inline int get_leave_quota_cnt(void)
{
    return (get_config_total_nurse());
}

static inline int get_long_term_leave_cnt(void)
{
    return (get_config_total_nurse());
}

static inline int get_green_line_leave_cnt(void)
{
    return (get_config_total_nurse());
}

static inline bool is_in_Stand_by_pool(ShiftCategory_t shift_c)
{
    ShiftCategory_t* pool = get_config_stand_by_pool();
    if(pool[shift_c] == true)
        return true;
    else
        return false;
}

static inline bool is_in_mid_shift_pool(ShiftCategory_t shift_c)
{
    ShiftCategory_t* pool = get_config_mid_shift_pool();
    if(pool[shift_c] == true)
        return true;
    else
        return false;
}

static inline int get_stand_by_demand_cnt(void)
{
    return (get_config_total_workday());
}

static inline int get_stand_by_leveling_cnt(void)
{
    int total_shift  = SHIFT_NUMBER;
    int* shift_nurse = get_config_shift_nurse();
    int shift_c, st_cnt = 0;
    for(shift_c = 0; shift_c < total_shift; shift_c++)
    {
        if(is_in_Stand_by_pool(shift_c))
            st_cnt += shift_nurse[shift_c];
    }
    return (st_cnt);
}

static inline int get_stand_by_limit_cnt(void)
{
    int total_shift  = SHIFT_NUMBER;
    int* shift_nurse = get_config_shift_nurse();
    int shift_c, st_cnt = 0;
    for(shift_c = 0; shift_c < total_shift; shift_c++)
    {
        if(!is_in_Stand_by_pool(shift_c))
            st_cnt += shift_nurse[shift_c];
    }
    return (st_cnt);
}

static inline int get_mid_shift_demand_cnt(void)
{
    return get_config_total_workday();
}

static inline int get_mid_shift_leveling_cnt(void)
{
    int total_shift  = SHIFT_NUMBER;
    int* shift_nurse = get_config_shift_nurse();
    int shift_c, st_cnt = 0;
    for(shift_c = 0; shift_c < total_shift; shift_c++)
    {
        if(is_in_mid_shift_pool(shift_c))
            st_cnt += shift_nurse[shift_c];
    }
    return (st_cnt);
}

static inline int get_mid_shift_limit_cnt(void)
{
    int total_shift  = SHIFT_NUMBER;
    int* shift_nurse = get_config_shift_nurse();
    int shift_c, st_cnt = 0;
    for(shift_c = 0; shift_c < total_shift; shift_c++)
    {
        if(!is_in_mid_shift_pool(shift_c))
            st_cnt += shift_nurse[shift_c];
    }
    return (st_cnt);
}

static inline int get_continuous_on_duty_cnt(void)
{
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    int cont_on_duty_limit = 0;
    int cont_on_duty_cnt   = 0;
    int* shift_nurse       = get_config_shift_nurse();
    int total_shift        = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        cont_on_duty_limit = get_cont_workday_limit(shift_c);
        if(last_month_p_total_workday > cont_on_duty_limit)
            cont_on_duty_cnt += (shift_nurse[shift_c] * (last_month_p_total_workday - cont_on_duty_limit));
    }
    return cont_on_duty_cnt;
}

static inline int get_off_duty_separate_cnt(void)
{
    int total_nurse   = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    return (total_nurse * ((total_workday) - 2));
}

static inline int get_min_off_duty_day_cnt(void)
{
    return (get_config_total_nurse());
}

static inline int get_weekend_leveling_cnt(void)
{
    return (get_config_total_nurse()-1); //nurse #0 always day off on weekend
}

static inline int get_shift_to_shift_cnt(void)
{
    return (get_config_total_nurse());
}

static inline int get_night_shift_15day_cnt(void)
{
    int* shift_nurse  = get_config_shift_nurse();
    return (shift_nurse[SHIFT_NIGHT] + shift_nurse[SHIFT_GRAVEYARD]);
}

static inline bool is_one_shift_cnt(void)
{
  if(get_config_total_shift() == 1)
        return true;
    else
        return false;
}

static inline HRStatus_t get_start_on_duty(ShiftCategory_t shift_category)
{
    if     (shift_category == SHIFT_DAY)        return HRSTATUS_CHARGE_ONDUTY_DAY;
    else if(shift_category == SHIFT_NIGHT)      return HRSTATUS_CHARGE_ONDUTY_NIGHT;
    else if(shift_category == SHIFT_GRAVEYARD)  return HRSTATUS_CHARGE_ONDUTY_GRAVEYARD;
    else
    {
        glp_assert(false);
        return 0;
    }
}

static inline HRStatus_t get_end_on_duty(ShiftCategory_t shift_category)
{
    if     (shift_category == SHIFT_DAY)        return HRSTATUS_STAFF_ONDUTY_DAY;
    else if(shift_category == SHIFT_NIGHT)      return HRSTATUS_STAFF_ONDUTY_NIGHT;
    else if(shift_category == SHIFT_GRAVEYARD)  return HRSTATUS_STAFF_ONDUTY_GRAVEYARD;
    else
    {
        glp_assert(false);
        return 0;
    }
}

static inline HRStatus_t get_charge_on_duty(ShiftCategory_t shift_category)
{
    if     (shift_category == SHIFT_DAY)        return HRSTATUS_CHARGE_ONDUTY_DAY;
    else if(shift_category == SHIFT_NIGHT)      return HRSTATUS_CHARGE_ONDUTY_NIGHT;
    else if(shift_category == SHIFT_GRAVEYARD)  return HRSTATUS_CHARGE_ONDUTY_GRAVEYARD;
    else
    {
        glp_assert(false);
        return 0;
    }
}

static inline HRStatus_t get_leader_on_duty(ShiftCategory_t shift_category)
{
    if     (shift_category == SHIFT_DAY)        return HRSTATUS_LEADER_ONDUTY_DAY;
    else if(shift_category == SHIFT_NIGHT)      return HRSTATUS_LEADER_ONDUTY_NIGHT;
    else if(shift_category == SHIFT_GRAVEYARD)  return HRSTATUS_LEADER_ONDUTY_GRAVEYARD;
    else
    {
        glp_assert(false);
        return 0;
    }
}

static inline HRStatus_t get_staff_on_duty(ShiftCategory_t shift_category)
{
    if     (shift_category == SHIFT_DAY)        return HRSTATUS_STAFF_ONDUTY_DAY;
    else if(shift_category == SHIFT_NIGHT)      return HRSTATUS_STAFF_ONDUTY_NIGHT;
    else if(shift_category == SHIFT_GRAVEYARD)  return HRSTATUS_STAFF_ONDUTY_GRAVEYARD;
    else
    {
        glp_assert(false);
        return 0;
    }
}

static inline HRStatus_t get_stand_by(ShiftCategory_t shift_category)
{
    if     (shift_category == SHIFT_DAY)        return HRSTATUS_STANDBY_DAY;
    else if(shift_category == SHIFT_NIGHT)      return HRSTATUS_STANDBY_NIGHT;
    else if(shift_category == SHIFT_GRAVEYARD)  return HRSTATUS_STANDBY_GRAVEYARD;
    else
    {
        glp_assert(false);
        return 0;
    }
}

static inline HRStatus_t get_start_off_duty(void)
{
    return HRSTATUS_RED_OFFDUTY;
}

static inline HRStatus_t get_end_off_duty(void)
{
    return HRSTATUS_LONG_OFFDUTY;
}

static inline bool is_on_duty(HRStatus_t HR_status, ShiftCategory_t shift_category)
{
    HRStatus_t on_duty_start = get_start_on_duty(shift_category);
    HRStatus_t on_duty_end   = get_end_on_duty(shift_category);
    
    if(on_duty_start <= HR_status && HR_status <= on_duty_end)
        return true;
    else
        return false;
}

static inline bool is_night_graveyard_shift(void)
{
    if(shift->shiftCategory == SHIFT_NIGHT || shift->shiftCategory == SHIFT_GRAVEYARD || get_config_total_shift() == 3)
        return true;
    else
        return false;
}

static inline bool is_any_on_duty(HRStatus_t HR_status)
{
    HRStatus_t on_duty_start_day       = get_start_on_duty(SHIFT_DAY);
    HRStatus_t on_duty_end_day         = get_end_on_duty(SHIFT_DAY);
    HRStatus_t on_duty_start_night     = get_start_on_duty(SHIFT_NIGHT);
    HRStatus_t on_duty_end_night       = get_end_on_duty(SHIFT_NIGHT);
    HRStatus_t on_duty_start_graveyard = get_start_on_duty(SHIFT_GRAVEYARD);
    HRStatus_t on_duty_end_graveyard   = get_end_on_duty(SHIFT_GRAVEYARD);
    
    if(on_duty_start_day <= HR_status && HR_status <= on_duty_end_day)
        return true;
    else if(on_duty_start_night <= HR_status && HR_status <= on_duty_end_night)
        return true;
    else if(on_duty_start_graveyard <= HR_status && HR_status <= on_duty_end_graveyard)
        return true;
    else
        return false;
}

static inline bool is_any_stand_by(HRStatus_t HR_status)
{
    HRStatus_t stand_by_day       = get_stand_by(SHIFT_DAY);
    HRStatus_t stand_by_night     = get_stand_by(SHIFT_NIGHT);
    HRStatus_t stand_by_graveyard = get_stand_by(SHIFT_GRAVEYARD);
    
    
    if(HR_status == stand_by_day || HR_status == stand_by_night || HR_status == stand_by_graveyard)
        return true;
    else
        return false;
}

static inline bool is_charge_on_duty(HRStatus_t HR_status, ShiftCategory_t shift_c)
{
    if(HR_status == get_charge_on_duty(shift_c))
        return true;
    else
        return false;
}

static inline bool is_leader_on_duty(HRStatus_t HR_status, ShiftCategory_t shift_c)
{
    if(HR_status == get_leader_on_duty(shift_c))
        return true;
    else
        return false;
}

static inline bool is_staff_on_duty(HRStatus_t HR_status, ShiftCategory_t shift_c)
{
    if(HR_status == get_staff_on_duty(shift_c))
        return true;
    else
        return false;
}

static inline bool is_off_duty(HRStatus_t HR_status)
{
    if((HR_status == HRSTATUS_RED_OFFDUTY) ||
       (HR_status == HRSTATUS_RED_ENHANCE_OFFDUTY) ||
       (HR_status == HRSTATUS_GREEN_OFFDUTY) ||
       (HR_status == HRSTATUS_LONG_OFFDUTY))
        return true;
    else
        return false;
}

static inline bool is_G_L_off_duty(HRStatus_t HR_status)
{
    if((HR_status == HRSTATUS_GREEN_OFFDUTY) ||
       (HR_status == HRSTATUS_LONG_OFFDUTY))
        return true;
    else
        return false;
}

static inline bool is_Red_off_duty(HRStatus_t HR_status)
{
    if(HR_status == HRSTATUS_RED_OFFDUTY)
        return true;
    else
        return false;
}

static inline bool is_Red_Enhance_off_duty(HRStatus_t HR_status)
{
    if(HR_status == HRSTATUS_RED_ENHANCE_OFFDUTY)
        return true;
    else
        return false;
}

static inline bool is_Green_off_duty(HRStatus_t HR_status)
{
    if(HR_status == HRSTATUS_GREEN_OFFDUTY)
        return true;
    else
        return false;
}

static inline bool is_Long_off_duty(HRStatus_t HR_status)
{
    if(HR_status == HRSTATUS_LONG_OFFDUTY)
        return true;
    else
        return false;
}

static inline bool is_specific_off_duty(HRStatus_t HR_status, int nurse, int day)
{
    if(is_Long_off_duty(staffs[nurse].AskLeaveList[day]))
        return true;
    else if(is_Red_Enhance_off_duty(staffs[nurse].AskLeaveList[day]))
        return true;
    else if(is_Red_off_duty(staffs[nurse].AskLeaveList[day]))
        return true;
    else if(is_Green_off_duty(staffs[nurse].AskLeaveList[day]))
        return true;
    else
        return false;
}

static inline bool is_Stand_by(HRStatus_t HR_status, ShiftCategory_t shift_c)
{
    if(HR_status == get_stand_by(shift_c))
        return true;
    else
        return false;
}

static inline bool is_Mid_on_duty(HRStatus_t HR_status)
{
    if(HR_status == HRSTATUS_MID_ONDUTY)
        return true;
    else
        return false;
}

static inline void get_on_duty_HRStatus(HRStatus_t* HRStatus_list, ShiftCategory_t shift_c, bool standby)
{
    HRStatus_t start = get_start_on_duty(shift_c);
    int on_cnt = get_config_on_duty_cnt();
    
    for(HRStatus_t cnt = start; cnt < (start + on_cnt); cnt++)
        *(HRStatus_list++) = cnt;
    
    if(standby == true)
        *(HRStatus_list++) = get_stand_by(shift_c);
}

static inline void get_off_duty_HRStatus(HRStatus_t* HRStatus_list, ShiftCategory_t shift_c, bool standby)
{
    HRStatus_t start = get_start_off_duty();
    int off_cnt = get_config_off_duty_cnt();
    
    if(standby == true)
        *HRStatus_list++ = get_stand_by(shift_c);
    
    for(HRStatus_t cnt = start; cnt < (start + off_cnt); cnt++)
        *HRStatus_list++ = cnt;
}

static inline void get_mid_shift_HRStatus(HRStatus_t* HRStatus_list)
{
    *HRStatus_list++ = HRSTATUS_MID_ONDUTY;
}

static inline void avoid_bound_values_same(int* lower, int* upper)
{
    if((*lower) == (*upper))
    {
        if((*lower) >= 1)   (*lower)--;
        else                (*upper)++;
    }
}

static inline void get_bound_per_poolnurse(HRStatus_t HR_status, int* bound)
{
    int* shift_nurse   = get_config_shift_nurse();
    int total_shift    = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0, poolnurse_cnt = 0;
    HRStatus_t lb_status = HRSTATUS_UNDEFINED;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        if(is_any_stand_by(HR_status))     lb_status = get_stand_by(shift_c);
        else if(is_Mid_on_duty(HR_status)) lb_status = HRSTATUS_MID_ONDUTY;
        else glp_assert(false);
        nurse_e += shift_nurse[shift_c];
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                if(is_nurse_grade_meet_position(nurse_c, lb_status, shift_c))
                    poolnurse_cnt++;
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
    double total_workday = get_config_total_workday();
    bound[0] = floor(total_workday/poolnurse_cnt);
    bound[1] = ceil(total_workday/poolnurse_cnt);
    avoid_bound_values_same(&bound[0], &bound[1]);
}

static inline int get_weekend_lower_bound(void)
{
    int total_weekend = shift->weekend[WEEKEND_CNT-1];
    return (total_weekend/3);
}

static inline int get_weekend_upper_bound(void)
{
    int total_weekend = shift->weekend[WEEKEND_CNT-1];
    return (total_weekend);
}

static inline int is_nurse_grade_meet_position(int nurse, HRStatus_t HR_status, ShiftCategory_t shift_c)
{
    bool* grade_fit_position = get_config_grade_fit_position();
    JobGrade_t grade = staffs[nurse].job_grade;
    JobPosition_t position = classify_job_position(HR_status);
    
    if(read_grade_fit_position_value(grade_fit_position, grade, position, shift_c) == true)
        return true;
    else
        return false;
}

static inline bool read_grade_fit_position_value(bool* grade_fit_position, JobGrade_t grade, JobPosition_t position,
                                                 ShiftCategory_t shift_c)
{
    return *(grade_fit_position + (grade * POSITION_NUMBER * SHIFT_NUMBER) + (position * SHIFT_NUMBER) + shift_c);
}

static inline JobPosition_t classify_job_position(HRStatus_t HR_status)
{
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        if(is_charge_on_duty(HR_status, shift_c))      return POSITION_CHARGE;
        else if(is_leader_on_duty(HR_status, shift_c)) return POSITION_LEADER;
        else if(is_staff_on_duty(HR_status, shift_c))  return POSITION_STAFF;
        else if(is_Stand_by(HR_status, shift_c))       return POSITION_STAND_BY;
        else if(is_Mid_on_duty(HR_status))             return POSITION_MID_SHIFT;
    }
    glp_assert(false);
    return POSITION_NUMBER;
}

//  ＮxDyW 代表人員x在工作天y的“工作變數”
//  ＮxDyL 代表人員x在工作天y的“休假變數”
//  work_var為true 代表針對ＮxDyW在objective function的係數做判斷
//  work_var為false代表針對ＮxDyL在objective function的係數做判斷
//  AskLeaveList[]為1代表該人員在該工作天要求休假
//  AskLeaveList[]為0代表該人員在該工作天未要求休假
//  ＮxDyW在有要求休假時配上負數的滿意度，未要求休假時配上普通滿意度
//  ＮxDyL在有要求休假時配上高度的滿意度，未要求休假時配上普通滿意度
static inline int feeling(HRStatus_t asking, HRStatus_t variable, ShiftCategory_t shift_c)
{
    int score = 0;
    int ask_idx = 0, var_idx = 0;
    
    if(asking == HRSTATUS_UNDEFINED)         ask_idx = 0;
    else if(is_on_duty(asking, shift_c))     ask_idx = 1;
    else if(is_Red_off_duty(asking))         ask_idx = 2;
    else if(is_Red_Enhance_off_duty(asking)) ask_idx = 3;
    else if(is_G_L_off_duty(asking))         ask_idx = 4;
    else if(is_Stand_by(asking, shift_c))    ask_idx = 5;
    else if(is_Mid_on_duty(asking))          ask_idx = 6;
    else glp_assert(false);
    
    if(is_any_on_duty(variable))               var_idx = 0;
    else if(is_Red_off_duty(variable))         var_idx = 1;
    else if(is_Red_Enhance_off_duty(variable)) var_idx = 2;
    else if(is_G_L_off_duty(variable))         var_idx = 3;
    else if(is_any_stand_by(variable))         var_idx = 4;
    else if(is_Mid_on_duty(variable))          var_idx = 5;
    else glp_assert(false);
    
    score = score_table[ask_idx][var_idx];
    
    return score;
}
static inline bool is_weekend(int day, int what_day_is_1st)
{
    int remainder_day = (day+1) % 7;
    if((remainder_day + what_day_is_1st == 7)// Saturday
       || (remainder_day + what_day_is_1st == 8)) // Sunday
        return true;
    else
        return false;
}
/*
static inline bool is_weekend(int day)
{
    int total_weekend  = shift->weekend[WEEKEND_CNT-1];
    
    for(int weekend_c = 0; weekend_c < total_weekend; weekend_c++)
    {
        if(day == shift->weekend[weekend_c])
            return true;
        
    }
    return false;
}
*/
/*
 回傳上月最後一天與本月第一天總計可上班最大天數
 */
static inline int is_1st_day_to_work(int lastM_shiftcategory, int lastM_lastD, int thisM_shiftcategory)
{
    //上月最後一天有上班
    //if(is_on_duty(lastM_lastD))
    HRStatus_t on_duty_start = get_start_on_duty(SHIFT_DAY);
    HRStatus_t on_duty_end   = get_end_on_duty(SHIFT_GRAVEYARD);
    
    if(on_duty_start <= lastM_lastD && lastM_lastD <= on_duty_end)
    {
        if((lastM_shiftcategory == SHIFT_DAY   && thisM_shiftcategory == SHIFT_GRAVEYARD) ||
           (lastM_shiftcategory == SHIFT_NIGHT && thisM_shiftcategory == SHIFT_DAY) ||
           (lastM_shiftcategory == SHIFT_NIGHT && thisM_shiftcategory == SHIFT_GRAVEYARD))
            return 1;//本月第一天不可上班
        else
            return 2;//本月第一天可上班
    }
    else//上月最後一天沒有上班
        return 1;//本月第一天可上班
}

static inline double get_red_enhancement_coefficient(int askLeaveCnt)
{
    int redEnhancementConstant = get_config_red_enhancement_constant();
    double askLeaveCnt_p_One = askLeaveCnt + 1;
    
    return ((askLeaveCnt + redEnhancementConstant) / askLeaveCnt_p_One);
}

static inline int getDay2Hour(int day)
{
    return (day * HOURS_PER_DAY);
}

static inline int getHour2FloorDay(int hour)
{
    return (hour / HOURS_PER_DAY);
}

static inline void memoryRelease(void* ptr)
{
    if(ptr != NULL)
    {
        free(ptr);
        ptr = NULL;
    }
}

#endif /* share_var_h */
