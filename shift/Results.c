//
//  Results.c
//  shift
//
//  Created by 張柏勛 on 2020/6/14.
//  Copyright © 2020 張柏勛. All rights reserved.
//

#include "Results.h"
AllResult_t* allResult = NULL;
SingleResult_t *currenSingleResult = NULL;

extern char* HRStatusResultPattern[HRSTATUS_NUMBER];
extern char* HRStatusAskPattern[ASKSTATUS_NUMBER];

void allResult_init(void)
{
    allResult                              = (AllResult_t*)calloc(1, sizeof(AllResult_t));
    allResult->backup_row_constraint_table = (tCONSTRAINT*)calloc(1, sizeof(row_constraint_table));
}

void allResult_free(void)
{
    memoryRelease(allResult->backup_row_constraint_table);
    memoryRelease(allResult);
}

void singleResult_init(int caseIndex)
{
    SingleResult_t* singleResult = &(allResult->singleResultList[caseIndex][strategy_idx]);
    singleResult->caseNo = caseIndex;
    singleResult->strategyNo = strategy_idx;
    singleResult->totalNurse = get_config_total_nurse();
    singleResult->totalWorkday = get_config_total_workday();
    currenSingleResult = singleResult;
}

void singleResult_saveValue(double finalValue)
{
    currenSingleResult->solutionValue = finalValue;
}

void singleResult_countDown(void)
{
    currenSingleResult->runTime.start = glp_time();
}

void singleResult_saveTime(void)
{
    RunTime_t* time = &currenSingleResult->runTime;
    time->end = glp_time();
    time->runTime = glp_difftime(time->end, time->start);
}

void singleResult_saveAskHRStatus(int nurse, int day, AskStatus_t ask)
{
    currenSingleResult->shiftAsk[nurse][day] = ask;
}

void singleResult_saveResultHRStatus(int nurse, int day, HRStatus_t hr)
{
    currenSingleResult->shiftResult[nurse][day] = hr;
}

void singleResult_initPersonnel(int unsatisfiedCnt)
{
    currenSingleResult->personnelCnt = unsatisfiedCnt;
}

void singleResult_savePersonnel(Staff_t* staff, int personnelIdx, int giveDay)
{
    UnSatisfied_t personnel;
    personnel.name         = staff->name;
    personnel.giveDay      = giveDay;
    personnel.askDay       = staff->askDayLeaves;
    personnel.totalMSP     = staff->modifiedScoreParameter[MSP_TOTAL];
    personnel.totalMSPRank = staff->rankOfLowMSP;
    memcpy(&currenSingleResult->personnelList[personnelIdx], &personnel, sizeof(UnSatisfied_t));
}

void singleResult_printHRStatus(void)
{
    SingleResult_t* singleResult = NULL;
    for(int caseIndex = 0; caseIndex < ProbCace_NUMBER; caseIndex++)
    {
        for(int strategyIndex = 0; strategyIndex < STRATEGY_CNT; strategyIndex++)
        {
            singleResult = &(allResult->singleResultList[caseIndex][strategyIndex]);
            if(singleResult->totalNurse == 0)
                continue;
            
            //solution info
            printf("Case: %d,  Strategy: %d,  Time: %.2f,  Unsatisfied: %d,  Value: %.2f\n", caseIndex, strategyIndex, singleResult->runTime.runTime,
                   singleResult->personnelCnt, singleResult->solutionValue);
#if 0
            for(int nurseIndex = 0; nurseIndex < singleResult->totalNurse; nurseIndex++)
            {
                UnSatisfied_t personnel = singleResult->personnelList[nurseIndex];
                
                //personnel
                printf("[%2d] %s Give:%d, Ask:%d, MSP:%f, MSPRank:%d\n", nurseIndex, personnel.name, personnel.giveDay, personnel.askDay, personnel.totalMSP, personnel.totalMSPRank);
                
                //Ask
                for(int workdayIndex = 0; workdayIndex < singleResult->totalWorkday; workdayIndex++)
                {
                    printf("%s", HRStatusAskPattern[singleResult->shiftAsk[nurseIndex][workdayIndex]]);
                }
                printf("\n");
                
                //Result
                for(int workdayIndex = 0; workdayIndex < singleResult->totalWorkday; workdayIndex++)
                {
                    printf("%s", HRStatusResultPattern[singleResult->shiftResult[nurseIndex][workdayIndex]]);
                }
                printf("\n");
            }
            printf("\n");
#endif
        }
        printf("\n");
    }
}
