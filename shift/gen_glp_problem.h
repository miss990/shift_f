//
//  gen_glp_prob.h
//  shift
//
//  Created by 張柏勛 on 2019/6/9.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#ifndef gen_glp_problem_h
#define gen_glp_problem_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "share_var.h"
#include "glpk.h"

typedef struct
{
    int* ith;
    int* jth;
    double* ij_array;
    int acture_element;
    int st_cnt;
    int active_constraint_cnt;
    int total_element;
}tGLP;

typedef struct tCONSTRAINT
{
    char* name;
    int (*cnt)(void);
    void (*function)(glp_prob*, tGLP*, int);
    int st_cnt_start;
    bool force;
    bool enable;
}tCONSTRAINT;

void gen_glp_problem(void);
void glp_check_constraint_enable(void);
void glp_array_init(tGLP* pGLP);
void glp_delete(tGLP* pGLP);
void glp_problem_creat(glp_prob *lp);
void glp_constraint_creat(glp_prob *lp, tGLP* pGLP);

void glp_row_cons_coef(glp_prob *lp, tGLP* pGLP);
void glp_row_unique_status         (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_staff_demand          (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_leader_demand         (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_charge_demand         (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_position_limit        (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_shift_limit           (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_leave_quota           (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_long_term_leave       (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_green_line_leave      (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_stand_by_demand       (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_stand_by_leveling     (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_stand_by_limit        (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_mid_shift_demand      (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_mid_shift_leveling    (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_mid_shift_limit       (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_continuous_on_duty    (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_off_duty_separate     (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_min_off_duty_day      (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_weekend_leveling      (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_shift_to_shift        (glp_prob *lp, tGLP* pGLP, int index);
void glp_row_night_shift_15day     (glp_prob *lp, tGLP* pGLP, int index);

void cal_obj_coef_modified_parameter(void);
void glp_col_constraint(glp_prob *lp, tGLP* pGLP);
double get_red_enhancement_coeff_on_date(double redEnhancementCoeff_nurse, HRStatus_t leave);
void glp_col_setfixbnds(glp_prob *lp, int var_cnt, int HR_status, int result);

void glp_load_and_sove(glp_prob *lp, tGLP* pGLP);
void glp_output_result(glp_prob *lp, tGLP* pGLP);
void glp_debug_log(glp_prob *lp, tGLP* pGLP);
void display_solution(glp_prob *lp, bool input, bool output);
void dispaly_weekend(void);
void display_input_HRStatus(int nurse_c, int day);
void display_solution_shift(ShiftCategory_t shift_c, int *shift_nurse);
void display_solution_input(glp_prob *lp, bool show_LM, int nurse_c, int* asktotal);
void display_solution_output(glp_prob *lp, bool show_LM, int nurse_c, int* asktotal);
bool display_solution_HRStatus(glp_prob *lp, int nurse_c, int day, HRStatus_t HR_Idx);
void display_solution_cal_hitrate(glp_prob *lp, int nurse_c, int* offduty, int* hitasktotal, int* asktotal);

void solution_xml(glp_prob *lp);

extern void singleResult_saveValue(double finalValue);
extern void singleResult_saveAskHRStatus(int nurse, int day, AskStatus_t ask);
extern void singleResult_saveResultHRStatus(int nurse, int day, HRStatus_t hr);
extern void singleResult_initPersonnel(int unsatisfiedCnt);
extern void singleResult_savePersonnel(Staff_t* staff, int personnelIdx, int giveDay);

extern tCONSTRAINT row_constraint_table[ROW_NUMBER];

static inline void glp_debug_st_log(int *idx, tGLP* pGLP)
{
    printf("i[%3d] %d    \tj[%3d] %d     \tij[%3d] %.0f\n", *idx, pGLP->ith[*idx], *idx, pGLP->jth[*idx], *idx, pGLP->ij_array[*idx]);
    (*idx)++;
}

static inline bool is_Stand_By_required(void)
{
    if((row_constraint_table[ROW_STAND_BY_DEMAND].enable == true) &&
       (row_constraint_table[ROW_STAND_BY_LEVELING].enable == true))
        return true;
    else
        return false;
}

static inline bool is_mid_shift_required(void)
{
    if((row_constraint_table[ROW_MID_SHIFT_DEMAND].enable == true) &&
       (row_constraint_table[ROW_MID_SHIFT_LEVELING].enable == true))
        return true;
    else
        return false;
}

#define           UNIQUE_STATUS (glp_row_unique_status)
#define            STAFF_DEMAND (glp_row_staff_demand)
#define           LEADER_DEMAND (glp_row_leader_demand)
#define           CHARGE_DEMAND (glp_row_charge_demand)
#define          POSITION_LIMIT (glp_row_position_limit)
#define             SHIFT_LIMIT (glp_row_shift_limit)
#define             LEAVE_QUOTA (glp_row_leave_quota)
#define         LONG_TERM_LEAVE (glp_row_long_term_leave)
#define        GREEN_LINE_LEAVE (glp_row_green_line_leave)
#define         STAND_BY_DEMAND (glp_row_stand_by_demand)
#define       STAND_BY_LEVELING (glp_row_stand_by_leveling)
#define          STAND_BY_LIMIT (glp_row_stand_by_limit)
#define        MID_SHIFT_DEMAND (glp_row_mid_shift_demand)
#define      MID_SHIFT_LEVELING (glp_row_mid_shift_leveling)
#define         MID_SHIFT_LIMIT (glp_row_mid_shift_limit)
#define      CONTINUOUS_ON_DUTY (glp_row_continuous_on_duty)
#define       OFF_DUTY_SEPARATE (glp_row_off_duty_separate)
#define        MIN_OFF_DUTY_DAY (glp_row_min_off_duty_day)
#define          SHIFT_TO_SHIFT (glp_row_shift_to_shift)
#define        WEEKEND_LEVELING (glp_row_weekend_leveling)
#define       NIGHT_SHIFT_15DAY (glp_row_night_shift_15day)
#define               UNSUPPORT (glp_row_unsupport)


#endif /* gen_glp_problem_h */
