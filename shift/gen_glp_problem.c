//
//  gen_glp_prob.c
//  shift
//
//  Created by 張柏勛 on 2019/6/9.
//  Copyright © 2019 張柏勛. All rights reserved.
//

#include "gen_glp_problem.h"

extern int problem_config[ProbCfg_NUMBER][ProbCace_NUMBER];
tCONSTRAINT row_constraint_table[ROW_NUMBER] =
{
    {"Unique Status",           get_unique_status_cnt,           UNIQUE_STATUS,          0,    false,    true},//L
    {"Staff Demand",            get_staff_demand_cnt,            STAFF_DEMAND,           0,    false,    true},
    {"Leader Demand",           get_leader_demand_cnt,           LEADER_DEMAND,          0,    false,    true},
    {"Charge Demand",           get_charge_demand_cnt,           CHARGE_DEMAND,          0,    false,    true},
    {"Position Limit",          get_position_limit_cnt,          POSITION_LIMIT,         0,    false,    true},
    {"Shift Limit",             get_shift_limit_cnt,             SHIFT_LIMIT,            0,    false,    true},
    {"Leave Quota",             get_leave_quota_cnt,             LEAVE_QUOTA,            0,    false,    true},
    {"Long Term Leave",         get_long_term_leave_cnt,         LONG_TERM_LEAVE,        0,    false,    true},
    {"Green Line Leave",        get_green_line_leave_cnt,        GREEN_LINE_LEAVE,       0,    false,    true},
    {"Stand By Demand",         get_stand_by_demand_cnt,         STAND_BY_DEMAND,        0,    false,    true},//STB_ENABLE},
    {"Stand By Leveling",       get_stand_by_leveling_cnt,       STAND_BY_LEVELING,      0,    false,    true},//STB_ENABLE},
    {"Stand By Limit",          get_stand_by_limit_cnt,          STAND_BY_LIMIT,         0,    false,    false},//!STB_ENABLE},
    {"Mid Shift Demand",        get_mid_shift_demand_cnt,        MID_SHIFT_DEMAND,       0,    false,    true},//MID_SHIFT_ENABLE},
    {"Mid Shift Leveling",      get_mid_shift_leveling_cnt,      MID_SHIFT_LEVELING,     0,    false,    true},//MID_SHIFT_ENABLE},
    {"Mid Shift Limit",         get_mid_shift_limit_cnt,         MID_SHIFT_LIMIT,        0,    false,    false},//!MID_SHIFT_ENABLE},
    {"Continuous On Duty",      get_continuous_on_duty_cnt,      CONTINUOUS_ON_DUTY,     0,    false,    true},//L
    {"Off Duty Separate",       get_off_duty_separate_cnt,       OFF_DUTY_SEPARATE,      0,    false,    true},
    {"Min Off Duty Day",        get_min_off_duty_day_cnt,        MIN_OFF_DUTY_DAY,       0,    false,    true},
    {"Weekend Leveling",        get_weekend_leveling_cnt,        WEEKEND_LEVELING,       0,    false,    true},
    {"Shift to Shift",          get_shift_to_shift_cnt,          SHIFT_TO_SHIFT,         0,    false,    false},//L
    {"Night Shift 15 Day",      get_night_shift_15day_cnt,       NIGHT_SHIFT_15DAY,      0,    false,    false}
};
tCONSTRAINT *backup_row_constraint_table;

char* HRStatusResultPattern[HRSTATUS_NUMBER] =
{
    " C",//HRSTATUS_CHARGE_ONDUTY_DAY
    " Ⱡ",//HRSTATUS_LEADER_ONDUTY_DAY
    " S",//HRSTATUS_STAFF_ONDUTY_DAY
    " T",//HRSTATUS_STANDBY_DAY
    " C",//HRSTATUS_CHARGE_ONDUTY_NIGHT
    " Ⱡ",//HRSTATUS_LEADER_ONDUTY_NIGHT
    " S",//HRSTATUS_STAFF_ONDUTY_NIGHT
    " T",//HRSTATUS_STANDBY_NIGHT
    " C",//HRSTATUS_CHARGE_ONDUTY_GRAVEYARD
    " Ⱡ",//HRSTATUS_LEADER_ONDUTY_GRAVEYARD
    " S",//HRSTATUS_STAFF_ONDUTY_GRAVEYARD
    " T",//HRSTATUS_STANDBY_GRAVEYARD
    " R",//HRSTATUS_RED_OFFDUTY
    " E",//HRSTATUS_RED_ENHANCE_OFFDUTY
    " G",//HRSTATUS_GREEN_OFFDUTY
    " L",//HRSTATUS_LONG_OFFDUTY
    " M"//HRSTATUS_MID_ONDUTY
};

char* HRStatusAskPattern[ASKSTATUS_NUMBER] =
{
    " _", //others
    " E",//HRSTATUS_RED_OFFDUTY Enhancement
    " Ŕ",//HRSTATUS_RED_OFFDUTY
    " G",//HRSTATUS_GREEN_OFFDUTY
    " Ĺ"//HRSTATUS_LONG_OFFDUTY
};

void gen_glp_problem(void)
{
    tGLP* pGLP = (tGLP*)calloc(1, sizeof(tGLP));//8
    glp_array_init(pGLP);
    //Problem objects
    glp_prob *lp = NULL;
    //create problem object
    lp = glp_create_prob();
    
    glp_problem_creat(lp);
    glp_constraint_creat(lp, pGLP);
    glp_load_and_sove(lp, pGLP);
    glp_debug_log(lp, pGLP);
    //solution_xml(lp);
    glp_delete_prob(lp);//erase problem object content
    glp_delete(pGLP);
}

void glp_array_init(tGLP* pGLP)
{
    int total_variable_cnt = get_config_total_variable_cnt();
    for(int cnt = 0; cnt < ROW_NUMBER; cnt++)
    {
        if(row_constraint_table[cnt].enable == true)
            pGLP->active_constraint_cnt += row_constraint_table[cnt].cnt();
    }
    pGLP->total_element = pGLP->active_constraint_cnt * total_variable_cnt;
    
    printf("Declare %d array size\n", pGLP->total_element+1);
    
    pGLP->ith = calloc(pGLP->total_element+1, sizeof(int));//9
    pGLP->jth = calloc(pGLP->total_element+1, sizeof(int));//10
    pGLP->ij_array = calloc(pGLP->total_element+1, sizeof(double));//11
}

void glp_delete(tGLP* pGLP)
{
    memoryRelease(pGLP->ij_array);//11.a
    memoryRelease(pGLP->jth);//10.a
    memoryRelease(pGLP->ith);//9.a
    memoryRelease(pGLP);//8.a
}

void glp_restore_constraint_table(void)
{
    if(backup_row_constraint_table == NULL)
    {
        backup_row_constraint_table = (tCONSTRAINT*)calloc(1, sizeof(row_constraint_table));//7
        memcpy(backup_row_constraint_table, row_constraint_table, sizeof(row_constraint_table));
    }
    else
        memcpy(row_constraint_table, backup_row_constraint_table, sizeof(row_constraint_table));
}

void glp_check_constraint_enable(void)
{
    glp_restore_constraint_table();
    //Leader
    ShiftCategory_t* demand = get_config_min_leader_per_workday();
    int cnt = demand[SHIFT_DAY] + demand[SHIFT_NIGHT] + demand[SHIFT_GRAVEYARD];
    if(cnt == 0 && row_constraint_table[ROW_LEADER_DEMAND].enable == true)
    {
        row_constraint_table[ROW_LEADER_DEMAND].force  = true;
        row_constraint_table[ROW_LEADER_DEMAND].enable = false;
    }
    
    demand = get_config_min_charge_per_workday();
    cnt = demand[SHIFT_DAY] + demand[SHIFT_NIGHT] + demand[SHIFT_GRAVEYARD];
    //Charge
    if(cnt == 0 && row_constraint_table[ROW_CHARGE_DEMAND].enable == true)
    {
        row_constraint_table[ROW_CHARGE_DEMAND].force  = true;
        row_constraint_table[ROW_CHARGE_DEMAND].enable = false;
    }
    
    //Shift
    if(get_config_total_shift() == 1 && row_constraint_table[ROW_SHIFT_LIMIT].enable == true)
    {
        row_constraint_table[ROW_SHIFT_LIMIT].force  = true;
        row_constraint_table[ROW_SHIFT_LIMIT].enable = false;
    }
    //Stand By SHOULD CHECK IN DIFFERENT CONDITIONS
    bool stand_by = get_config_stand_by_bool();
    row_constraint_table[ROW_STAND_BY_DEMAND].enable   = stand_by;
    row_constraint_table[ROW_STAND_BY_LEVELING].enable = stand_by;
    row_constraint_table[ROW_STAND_BY_LIMIT].enable    = !stand_by;
    if(get_config_total_shift() == 3 && !row_constraint_table[ROW_STAND_BY_LIMIT].enable && stand_by)
    {
        row_constraint_table[ROW_STAND_BY_LIMIT].force  = true;
        row_constraint_table[ROW_STAND_BY_LIMIT].enable = true;
    }
    
    //Mid shift
    bool mid_shift = get_config_mid_shift_bool();
    row_constraint_table[ROW_MID_SHIFT_DEMAND].enable   = mid_shift;
    row_constraint_table[ROW_MID_SHIFT_LEVELING].enable = mid_shift;
    row_constraint_table[ROW_MID_SHIFT_LIMIT].enable    = !mid_shift;
    demand = get_config_mid_shift_pool();
    cnt = demand[SHIFT_DAY] + demand[SHIFT_NIGHT] + demand[SHIFT_GRAVEYARD];
    if(cnt == 0 && row_constraint_table[ROW_MID_SHIFT_DEMAND].enable == true)
    {
        row_constraint_table[ROW_MID_SHIFT_DEMAND].force  = true;
        row_constraint_table[ROW_MID_SHIFT_DEMAND].enable = false;
    }
    
    //Night Shift
    if(is_night_graveyard_shift() ^ row_constraint_table[ROW_NIGHT_SHIFT_15DAY].enable)
    {
        row_constraint_table[ROW_NIGHT_SHIFT_15DAY].force  = true;
        row_constraint_table[ROW_NIGHT_SHIFT_15DAY].enable = is_night_graveyard_shift();
    }
}

void glp_problem_creat(glp_prob *lp)
{
    //assign (change) problem name
    glp_set_prob_name(lp, "Shifting");
    
    //set (change) optimization direction flag
    glp_set_obj_dir(lp, GLP_MAX);
}

void glp_constraint_creat(glp_prob *lp, tGLP* pGLP)
{
    int total_nurse   = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    glp_assert(total_nurse <= NURSE_ARRAY_SUPPORT);
    glp_assert(total_workday <= WORKDAY_ARRAY_SUPPORT);
    glp_row_cons_coef(lp, pGLP);
    cal_obj_coef_modified_parameter();
    glp_col_constraint(lp, pGLP);
}

void glp_row_cons_coef(glp_prob *lp, tGLP* pGLP)
{
    pGLP->st_cnt = 1;
    pGLP->acture_element = 1;
    char* force = "Force ";
    //int a=1;
    
    for(int index=0; index<ROW_NUMBER; index++)
    {
        if(row_constraint_table[index].enable)
        {
            if(row_constraint_table[index].force) printf("%s", force);
            printf("Enable %s %d\n", row_constraint_table[index].name, row_constraint_table[index].cnt());
            glp_add_rows(lp, row_constraint_table[index].cnt());
            row_constraint_table[index].st_cnt_start = pGLP->st_cnt;
            row_constraint_table[index].function(lp, pGLP, index);
            //a+=row_constraint_table[index].cnt();
            //if(a != st_cnt) printf("C %d A %d\n", a, st_cnt);
        }
        else
        {
            if(row_constraint_table[index].force) printf("%s", force);
            printf("Disable %s\n", row_constraint_table[index].name);
        }
    }
    printf("\n");
    
    pGLP->acture_element--;
    glp_assert(pGLP->st_cnt-1 == glp_get_num_rows(lp));
    glp_assert(pGLP->st_cnt-1 == pGLP->active_constraint_cnt);
}

void glp_add_row(glp_prob *lp, tGLP* pGLP, int index, int boundtype, int lb, int up)
{
    char nurse_c[80] = "";
    st_num2char(pGLP->st_cnt, row_constraint_table[index].name, nurse_c);
    glp_set_row_name(lp, pGLP->st_cnt, nurse_c);
    glp_set_row_bnds(lp, pGLP->st_cnt, boundtype, lb, up);
}

void glp_add_element(tGLP* pGLP, int nurse, int day, HRStatus_t HR_status, double coef)
{
    pGLP->ith[pGLP->acture_element]      = pGLP->st_cnt;
    pGLP->jth[pGLP->acture_element]      = col_map(nurse, day, HR_status);
    pGLP->ij_array[pGLP->acture_element] = coef;
    pGLP->acture_element++;
}

void glp_row_unique_status(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. unique_status    TOTAL_NURSE * TOTAL_WORKDAY
    int DayIncludeLastMonth;
    int total_workday = get_config_total_workday();
    int on_duty_cnt   = get_config_on_duty_cnt();
    int stand_by_cnt  = get_config_stand_by_cnt();
    int off_duty_cnt  = get_config_off_duty_cnt();
    int mid_shift_cnt = get_config_mid_shift_cnt();
    int status_cnt    = on_duty_cnt + stand_by_cnt + off_duty_cnt + mid_shift_cnt;
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(status_cnt, sizeof(HRStatus_t));
        
        get_on_duty_HRStatus(HRStatus_list, shift_c, false);
        get_off_duty_HRStatus(HRStatus_list + on_duty_cnt, shift_c, true);
        get_mid_shift_HRStatus(HRStatus_list + (status_cnt-mid_shift_cnt));
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            for(int day = 0; day < total_workday; day++)
            {
                glp_add_row(lp, pGLP, index, GLP_FX, 1, 1);
                
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                
                for(int cnt = 0; cnt < status_cnt; cnt++)
                {
                    HRStatus_t HR_status = HRStatus_list[cnt];
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, HR_status, 1.0);
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
}

void glp_row_staff_demand(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. staff_demand    TOTAL_WORKDAY
    int DayIncludeLastMonth;
    int total_workday = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        for(int day = 0; day < total_workday; day++)
        {
            glp_add_row(lp, pGLP, index, GLP_FX, shift->minRequiredStaff[shift_c][day], shift->minRequiredStaff[shift_c][day]);
            
            DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
            
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                if(is_nurse_grade_meet_position(nurse_c, get_staff_on_duty(shift_c), shift_c))
                {
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, get_staff_on_duty(shift_c), 1.0);
                }
            }
            (pGLP->st_cnt)++;
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_leader_demand(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. leader_demand    TOTAL_WORKDAY
    int DayIncludeLastMonth;
    int total_workday          = get_config_total_workday();
    ShiftCategory_t* min_leader_per_workday = get_config_min_leader_per_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(min_leader_per_workday[shift_c] > 0)
        {
            for(int day = 0; day < total_workday; day++)
            {
                glp_add_row(lp, pGLP, index, GLP_FX, min_leader_per_workday[shift_c], min_leader_per_workday[shift_c]);
                
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                
                for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
                {
                    if(is_nurse_grade_meet_position(nurse_c, get_leader_on_duty(shift_c), shift_c))
                    {
                        glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, get_leader_on_duty(shift_c), 1.0);
                    }
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_charge_demand(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. charge_demand    TOTAL_WORKDAY
    int DayIncludeLastMonth;
    int total_workday          = get_config_total_workday();
    ShiftCategory_t* min_charge_per_workday = get_config_min_charge_per_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(min_charge_per_workday[shift_c] > 0)
        {
            for(int day = 0; day < total_workday; day++)
            {
                glp_add_row(lp, pGLP, index, GLP_FX, min_charge_per_workday[shift_c], min_charge_per_workday[shift_c]);
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                
                for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
                {
                    if(is_nurse_grade_meet_position(nurse_c, get_charge_on_duty(shift_c), shift_c))
                    {
                        glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, get_charge_on_duty(shift_c), 1.0);
                    }
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_position_limit(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. position_limit      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_workday = get_config_total_workday();
    int on_duty_cnt   = get_config_on_duty_cnt() + get_config_stand_by_cnt();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
        get_on_duty_HRStatus(HRStatus_list, shift_c, true);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
            
            for(int cnt = 0; cnt < on_duty_cnt; cnt++)
            {
                HRStatus_t on = HRStatus_list[cnt];
                if(is_nurse_grade_meet_position(nurse_c, on, shift_c))
                    continue;
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, on, 1.0);
                }
            }
            (pGLP->st_cnt)++;
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
}

void glp_row_shift_limit(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. shift_limit      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_nurse       = get_config_total_nurse();
    int total_workday     = get_config_total_workday();
    int other_on_duty_cnt = get_config_on_duty_cnt() + get_config_stand_by_cnt();
    int total_shift       = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    ShiftCategory_t def_shift;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        def_shift = staffs[nurse].default_shift;
        for(shift_c = shift_s; shift_c < shift_e; shift_c++)
        {
            if(shift_c == def_shift) continue;
            glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
            
            HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(other_on_duty_cnt, sizeof(HRStatus_t));
            get_on_duty_HRStatus(HRStatus_list, shift_c, true);
            
            for(int day = 0; day < total_workday; day++)
            {
                for(int cnt = 0; cnt < other_on_duty_cnt; cnt++)
                {
                    HRStatus_t other = HRStatus_list[cnt];
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    glp_add_element(pGLP, nurse, DayIncludeLastMonth, other, 1.0);
                }
            }
            free(HRStatus_list);
        }
        (pGLP->st_cnt)++;
    }
}

void glp_row_leave_quota(glp_prob *lp, tGLP* pGLP, int index)
{
    int DayIncludeLastMonth;
    int total_workday = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt  = get_config_off_duty_cnt();
        bool stand_by = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            glp_add_row(lp, pGLP, index, GLP_UP, 0, getHour2FloorDay(staffs[nurse_c].leaveQuota_Hour));
            
            for(int day = 0; day < total_workday; day++)
            {
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                
                for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                {
                    HRStatus_t off = HRStatus_list[cnt];
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, off, 1.0);
                }
            }
            (pGLP->st_cnt)++;
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
}

void glp_row_long_term_leave(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. long_term_leave      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_nurse   = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        if(staffs[nurse].longtermleaveask > 0)
            glp_add_row(lp, pGLP, index, GLP_UP, 0, staffs[nurse].longtermleaveask);
        else
            glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
        
        for(int day = 0; day < total_workday; day++)
        {
            DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
            glp_add_element(pGLP, nurse, DayIncludeLastMonth, HRSTATUS_LONG_OFFDUTY, 1.0);
        }
        (pGLP->st_cnt)++;
    }
}

void glp_row_green_line_leave(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. green_line_leave      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_nurse   = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        if(staffs[nurse].greenleaveask > 0)
            glp_add_row(lp, pGLP, index, GLP_UP, 0, staffs[nurse].greenleaveask);
        else
            glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
        
        for(int day = 0; day < total_workday; day++)
        {
            DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
            glp_add_element(pGLP, nurse, DayIncludeLastMonth, HRSTATUS_GREEN_OFFDUTY, 1.0);
        }
        (pGLP->st_cnt)++;
    }
}

void glp_row_stand_by_demand(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. stand_by_demand    TOTAL_WORKDAY
    int DayIncludeLastMonth;
    int total_nurse              = get_config_total_nurse();
    int total_workday            = get_config_total_workday();
    int min_stand_by_per_workday = 1;
    int shift_c;
    int nurse_c;

    for(int day = 0; day < total_workday; day++)
    {
        glp_add_row(lp, pGLP, index, GLP_FX, min_stand_by_per_workday, min_stand_by_per_workday);
        
        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
        for(nurse_c = 0; nurse_c < total_nurse; nurse_c++)
        {
            shift_c = staffs[nurse_c].default_shift;
            if(is_in_Stand_by_pool(shift_c))
            {
                if(is_nurse_grade_meet_position(nurse_c, get_stand_by(shift_c), shift_c))
                {
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, get_stand_by(shift_c), 1.0);
                }
            }
        }
        (pGLP->st_cnt)++;
    }
}

void glp_row_stand_by_leveling(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. stand_by_leveling      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_workday  = get_config_total_workday();
    int* shift_nurse   = get_config_shift_nurse();
    int total_shift    = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    int stand_by_bound[2] = {0};
    get_bound_per_poolnurse(HRSTATUS_STANDBY_DAY, stand_by_bound);
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(is_in_Stand_by_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                if(is_nurse_grade_meet_position(nurse_c, get_stand_by(shift_c), shift_c))
                    glp_add_row(lp, pGLP, index, GLP_DB, stand_by_bound[0], stand_by_bound[1]);
                else
                    glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, get_stand_by(shift_c), 1.0);
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_stand_by_limit(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. stand_by_limit      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_workday  = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(!is_in_Stand_by_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, get_stand_by(shift_c), 1.0);
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_mid_shift_demand(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. mid_shift_demand    TOTAL_WORKDAY
    int DayIncludeLastMonth;
    int total_nurse               = get_config_total_nurse();
    int total_workday             = get_config_total_workday();
    int min_mid_shift_per_workday = 1;
    int first_day                 = get_config_first_day_is();
    int shift_c;
    int nurse_c;
    
    for(int day = 0; day < total_workday; day++)
    {
        if(is_weekend(day, first_day))
            glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
        else
            glp_add_row(lp, pGLP, index, GLP_FX, min_mid_shift_per_workday, min_mid_shift_per_workday);
        
        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
        for(nurse_c = 0; nurse_c < total_nurse; nurse_c++)
        {
            shift_c = staffs[nurse_c].default_shift;
            if(is_in_mid_shift_pool(shift_c))
            {
                if(is_nurse_grade_meet_position(nurse_c, HRSTATUS_MID_ONDUTY, shift_c))
                {
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, HRSTATUS_MID_ONDUTY, 1.0);
                }
            }
        }
        (pGLP->st_cnt)++;
    }
}

void glp_row_mid_shift_leveling(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. mid_shift_leveling      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_workday  = get_config_total_workday();
    int* shift_nurse   = get_config_shift_nurse();
    int total_shift    = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    int mid_shift_bound[2] = {0};
    get_bound_per_poolnurse(HRSTATUS_MID_ONDUTY, mid_shift_bound);
    
    mid_shift_bound[0] = 1;//Workaround 5

    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(is_in_mid_shift_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                if(is_nurse_grade_meet_position(nurse_c, HRSTATUS_MID_ONDUTY, shift_c))
                    glp_add_row(lp, pGLP, index, GLP_DB, mid_shift_bound[0], mid_shift_bound[1]);
                else
                    glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, HRSTATUS_MID_ONDUTY, 1.0);
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_mid_shift_limit(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. mid_shift_limit      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_workday  = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(!is_in_mid_shift_pool(shift_c))
        {
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                glp_add_row(lp, pGLP, index, GLP_FX, 0, 0);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, HRSTATUS_MID_ONDUTY, 1.0);
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_continuous_on_duty(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. continuous_on_duty  TOTAL_NURSE * (6+TOTAL_WORKDAY-cont_on_duty_limit)
    int day_range;
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    int* shift_nurse               = get_config_shift_nurse();
    int total_shift                = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int on_duty_cnt            = get_config_on_duty_cnt();
        bool stand_by              = is_Stand_By_required();
        bool mid_shift             = is_mid_shift_required();
        if(stand_by)  on_duty_cnt += get_config_stand_by_cnt();
        int mid_cnt                = get_config_mid_shift_cnt();
        if(mid_shift) on_duty_cnt += mid_cnt;
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
        get_on_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        if(mid_shift) get_mid_shift_HRStatus(HRStatus_list + (on_duty_cnt-mid_cnt));
        int cont_on_duty_limit = get_cont_workday_limit(shift_c);
        int sets_to_consider   = (last_month_p_total_workday - cont_on_duty_limit);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            for(int day = 0; day < sets_to_consider; day++)
            {
                glp_add_row(lp, pGLP, index, GLP_UP, 0, cont_on_duty_limit);
                
                //Only allow up to [cont_on_duty_limit] days of on duty in any [cont_on_duty_limit + 1] days
                day_range = (day + cont_on_duty_limit + 1);
                for(int range_cnt = day; range_cnt < day_range; range_cnt++)
                {
                    for(int cnt = 0; cnt < on_duty_cnt; cnt++)
                    {
                        HRStatus_t on = HRStatus_list[cnt];
                        glp_add_element(pGLP, nurse_c, range_cnt, on, 1.0);
                    }
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
}

void glp_row_off_duty_separate(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. off_duty_seperate   TOTAL_NURSE * (TOTAL_WORKDAY-2)
    int DayIncludeLastMonth, day_range;
    int total_workday       = get_config_total_workday();
    int sets_to_consider    = (total_workday-2);
    int* shift_nurse               = get_config_shift_nurse();
    int total_shift                = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt        = get_config_on_duty_cnt();
        bool stand_by           = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            for(int day = 0; day < sets_to_consider; day++)
            {
                glp_add_row(lp, pGLP, index, GLP_DB, -1, 1);
                
                day_range = day + 3;
                for(int range_cnt = day; range_cnt < day_range; range_cnt++)
                {
                    for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                    {
                        HRStatus_t off = HRStatus_list[cnt];
                        DayIncludeLastMonth = get_day_plus_last_6workday_last_month(range_cnt);
                        if(range_cnt == day+1) glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, off, -1.0);
                        else                   glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, off, 1.0);
                    }
                }
                (pGLP->st_cnt)++;
            }
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
}

void glp_row_min_off_duty_day(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. min_off_duty_day      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_workday = get_config_total_workday();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int MaxOffDutyDay = get_config_max_off_duty_per_staff();
    int MinOffDutyDay = get_config_min_off_duty_per_staff();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
        //MinOffDutyDay = shift->MinOffDutyDay;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt           = get_config_off_duty_cnt();
        bool stand_by              = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            glp_add_row(lp, pGLP, index, GLP_DB, MinOffDutyDay, MaxOffDutyDay);
            
            for(int day = 0; day < total_workday; day++)
            {
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                {
                    HRStatus_t off = HRStatus_list[cnt];
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, off, 1.0);
                }
            }
            (pGLP->st_cnt)++;
        }
        nurse_s += shift_nurse[shift_c];
        free(HRStatus_list);
    }
}

void glp_row_weekend_leveling(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. weekend_leveling      TOTAL_NURSE
    int DayIncludeLastMonth, day;
    int total_weekend  = shift->weekend[WEEKEND_CNT-1];
    int weekend_lb     = get_weekend_lower_bound();
    int weekend_ub     = get_weekend_upper_bound();
    int* shift_nurse   = get_config_shift_nurse();
    int total_shift    = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        int off_duty_cnt           = get_config_off_duty_cnt();
        bool stand_by              = is_Stand_By_required();
        if(stand_by) off_duty_cnt += get_config_stand_by_cnt();
        HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(off_duty_cnt, sizeof(HRStatus_t));
        get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
        
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            if(nurse_c == 0) continue;//nurse #0 always day off on weekend
            glp_add_row(lp, pGLP, index, GLP_DB, weekend_lb, weekend_ub);
            
            for(int weekend_c = 0; weekend_c < total_weekend; weekend_c++)
            {
                day = shift->weekend[weekend_c];
                DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                for(int cnt = 0; cnt < off_duty_cnt; cnt++)
                {
                    HRStatus_t off = HRStatus_list[cnt];
                    glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, off, 1.0);
                }
            }
            (pGLP->st_cnt)++;
        }
        free(HRStatus_list);
        nurse_s += shift_nurse[shift_c];
    }
}

void glp_row_shift_to_shift(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. shift_to_shift      TOTAL_NURSE
    int lastM_shiftcategory, lastM_lastD, thisM_shiftcategory, sts;
    int total_nurse              = get_config_total_nurse();
    int last_6workday_last_month = get_config_last_6workday_last_month();
    int day_start                = last_6workday_last_month-1;
    int day_end                  = last_6workday_last_month;
    int on_duty_cnt              = get_config_on_duty_cnt();
    ShiftCategory_t shift_c      = shift->shiftCategory;
    bool stand_by                = is_Stand_By_required();
    
    if(stand_by) on_duty_cnt    += get_config_stand_by_cnt();
    
    HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
    get_off_duty_HRStatus(HRStatus_list, shift_c, stand_by);
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        //上月最後一天與本月第一天，依據班別組合，最多可以是兩天都上班
        lastM_shiftcategory = staffs[nurse].lastmonth_shift;
        lastM_lastD         = staffs[nurse].LMShiftList[last_6workday_last_month-1];
        thisM_shiftcategory = shift->shiftCategory;
        sts                 = is_1st_day_to_work(lastM_shiftcategory, lastM_lastD, thisM_shiftcategory);
        glp_add_row(lp, pGLP, index, GLP_UP, 0, sts);
        
        //上月最後一天與本月第一天
        for(int DayIncludeLastMonth = day_start; DayIncludeLastMonth <= day_end; DayIncludeLastMonth++)
        {
            for(int cnt = 0; cnt < on_duty_cnt; cnt++)
            {
                HRStatus_t on = HRStatus_list[cnt];
                glp_add_element(pGLP, nurse, DayIncludeLastMonth, on, 1.0);
            }
        }
        (pGLP->st_cnt)++;
    }
    free(HRStatus_list);
}

void glp_row_night_shift_15day(glp_prob *lp, tGLP* pGLP, int index)
{
    //s.t. night_shift_15D      TOTAL_NURSE
    int DayIncludeLastMonth;
    int total_workday                = get_config_total_workday();
    int min_on_duty_days_night_shift = get_config_min_on_duty_days_night_shift();
    int on_duty_cnt                  = get_config_on_duty_cnt();
    int* shift_nurse                 = get_config_shift_nurse();
    int total_shift                  = get_config_total_shift();
    int shift_c, shift_s = 0, shift_e = SHIFT_NUMBER;
    int nurse_c, nurse_s = 0, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        if(shift_c != SHIFT_DAY)
        {
            HRStatus_t* HRStatus_list = (HRStatus_t*)calloc(on_duty_cnt, sizeof(HRStatus_t));
            get_on_duty_HRStatus(HRStatus_list, shift_c, false);
            
            for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
            {
                glp_add_row(lp, pGLP, index, GLP_LO, min_on_duty_days_night_shift, min_on_duty_days_night_shift);
                
                for(int day = 0; day < total_workday; day++)
                {
                    DayIncludeLastMonth = get_day_plus_last_6workday_last_month(day);
                    
                    for(int cnt = 0; cnt < on_duty_cnt; cnt++)
                    {
                        HRStatus_t on = HRStatus_list[cnt];
                        glp_add_element(pGLP, nurse_c, DayIncludeLastMonth, on, 1.0);
                    }
                }
                (pGLP->st_cnt)++;
            }
            free(HRStatus_list);
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void cal_obj_coef_modified_parameter(void)
{
    int minAskDayLeave = MAX_INT;
    int maxRemainHourOwingLeave = MIN_INT;
    int total_nurse = get_config_total_nurse();
    
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        if(staffs[nurse].askDayLeaves < minAskDayLeave)
            minAskDayLeave = staffs[nurse].askDayLeaves;
        
        if(staffs[nurse].remainHourOwingLeave > maxRemainHourOwingLeave)
            maxRemainHourOwingLeave = staffs[nurse].remainHourOwingLeave;
    }
    if(minAskDayLeave == 0) minAskDayLeave = 1;
    if(maxRemainHourOwingLeave == 0) maxRemainHourOwingLeave = HOURS_PER_DAY;
    
    double askCnt, remainOwing, remainAnnual, total;
    double ShouldNotZero;
    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        ShouldNotZero = staffs[nurse].askDayLeaves > 1 ? staffs[nurse].askDayLeaves : 1;
        askCnt       = (double)minAskDayLeave/ShouldNotZero;
        
        ShouldNotZero = staffs[nurse].remainHourOwingLeave > HOURS_PER_DAY ? staffs[nurse].remainHourOwingLeave : HOURS_PER_DAY;
        remainOwing  = (double)ShouldNotZero/maxRemainHourOwingLeave;
        
        ShouldNotZero = staffs[nurse].remainHourAnnualLeave > HOURS_PER_DAY ? staffs[nurse].remainHourAnnualLeave : HOURS_PER_DAY;
        remainAnnual = (double)ShouldNotZero/staffs[nurse].completeHourAnnualLeave;
        
        total = (askCnt * MSP_ASK_CNT_PORTION + remainOwing * MSP_REMAIN_OWING_PORTION + remainAnnual * MSP_REMAIN_ANNUAL_PORTION);
        total /= MSP_TOTAL_PORTION;
        
        staffs[nurse].modifiedScoreParameter[MSP_ASK_CNT]       = askCnt;
        staffs[nurse].modifiedScoreParameter[MSP_REMAIN_OWING]  = remainOwing;
        staffs[nurse].modifiedScoreParameter[MSP_REMAIN_ANNUAL] = remainAnnual;
        staffs[nurse].modifiedScoreParameter[MSP_TOTAL]         = total;
        //printf("askcnt %.2f, r_owing %.2f, r_annual %.2f, t %.2f\n", askCnt, remainOwing, remainAnnual, total);
    }
    double buffer[NURSE_ARRAY_SUPPORT];
    for(int nurse = 0; nurse < total_nurse; nurse++)
        buffer[nurse] = staffs[nurse].modifiedScoreParameter[MSP_TOTAL];
    for(int loop1=0; loop1<total_nurse; loop1++)
    {
        for(int loop2=loop1+1; loop2<total_nurse; loop2++)
        {
            if(buffer[loop1] > buffer[loop2])
            {
                double smaller = buffer[loop2];
                buffer[loop2] = buffer[loop1];
                buffer[loop1] = smaller;
            }
        }
        //printf("[%d]: %f\n", loop1, buffer[loop1]);
    }
    for(int rank=0; rank<total_nurse; rank++)
    {
        if(rank>0 && buffer[rank-1] == buffer[rank]) continue;
        for(int nurse=0; nurse<total_nurse; nurse++)
        {
            if(buffer[rank] == staffs[nurse].modifiedScoreParameter[MSP_TOTAL])
                staffs[nurse].rankOfLowMSP = rank;
        }
    }
}

void glp_col_constraint(glp_prob *lp, tGLP* pGLP)
{
    int var_cnt = 1, day;
    int total_nurse                = get_config_total_nurse();
    int total_variable_cnt         = get_config_total_variable_cnt();
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    int last_6workday_last_month   = get_config_last_6workday_last_month();
    double score, modifiedScoreParameter, redEnhancementCoeff, redEnhancementCoeff_nurse;
    ShiftCategory_t shift_c;
    
    //add new columns (structural variables) to problem object
    glp_add_cols(lp, total_variable_cnt);

    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        shift_c = staffs[nurse].default_shift;
        modifiedScoreParameter    = staffs[nurse].modifiedScoreParameter[MSP_TOTAL];
        redEnhancementCoeff_nurse = get_red_enhancement_coefficient(staffs[nurse].askDayRedLeave);
        for(int DayIncludeLastMonth = 0; DayIncludeLastMonth < last_month_p_total_workday; DayIncludeLastMonth++)
        {
            int dayThisMonth = get_day_minus_last_6workday_last_month(DayIncludeLastMonth);
            HRStatus_t leave = staffs[nurse].AskLeaveList[dayThisMonth];

            redEnhancementCoeff = get_red_enhancement_coeff_on_date(redEnhancementCoeff_nurse, leave);
                
            for(int HR_status = 0; HR_status < HRSTATUS_NUMBER; HR_status++)
            {
                char var_c[80] = "";
                var_num2char(nurse, DayIncludeLastMonth, var_c, HR_status, true);
                //assign (change) column name to j-th column (structural variable)
                glp_set_col_name(lp, var_cnt, var_c);
                
                //set (change) column bounds of j-th column (structural variable)
                if(DayIncludeLastMonth < last_6workday_last_month)//results of last 6 days of last month
                {
                    int result = staffs[nurse].LMShiftList[DayIncludeLastMonth];
                    glp_col_setfixbnds(lp, var_cnt, HR_status, result);
                    //set (change) objective coefficient or constant term at j-th column (structura//l variable)
                    //glp_set_obj_coef(lp, var_cnt, feeling(staffs[nurse].LMShiftList[DayIncludeLastMonth], HR_status));
                    glp_set_obj_coef(lp, var_cnt, 0);
                }
                else//requests of current month
                {
                    glp_set_col_bnds(lp, var_cnt, GLP_DB, 0.0, 1.0);
                    day = get_day_minus_last_6workday_last_month(DayIncludeLastMonth);
                    //set (change) objective coefficient or constant term at j-th column (structura//l variable)
                    score = feeling(staffs[nurse].AskLeaveList[day], HR_status, shift_c)
                        * modifiedScoreParameter * redEnhancementCoeff;
                    glp_set_obj_coef(lp, var_cnt, score);
                }
                //set (change) column kind
                glp_set_col_kind(lp, var_cnt, GLP_IV);
                var_cnt++;
            }
        }
    }
    glp_assert(var_cnt-1 == glp_get_num_cols(lp));
    glp_assert(var_cnt-1 == total_variable_cnt);
}

double get_red_enhancement_coeff_on_date(double redEnhancementCoeff_nurse, HRStatus_t leave)
{
    double redEnhancementCoeff;

    if(leave == HRSTATUS_RED_ENHANCE_OFFDUTY)
        redEnhancementCoeff = redEnhancementCoeff_nurse;
    else
        redEnhancementCoeff = CONSTANT_ONE;
    
    return redEnhancementCoeff;
}

void glp_col_setfixbnds(glp_prob *lp, int var_cnt, int HR_status, int result)
{
    int bnd = (result == HR_status) ? 1 : 0;
    glp_set_col_bnds(lp, var_cnt, GLP_FX, bnd, bnd);
}

void glp_load_and_sove(glp_prob *lp, tGLP* pGLP)
{
    //load (replace) the whole constraint matrix, where ia[k] is the row index, ja[k] is the column index, and ar[k] is a numeric value of corresponding constraint coefficient
    int total_variable_cnt = get_config_total_variable_cnt();
    glp_assert(pGLP->ith[pGLP->acture_element+1]==0);
    glp_assert(pGLP->jth[pGLP->acture_element+1]==0);
    glp_assert(pGLP->ij_array[pGLP->acture_element+1]==0);
    glp_assert(glp_get_num_cols(lp)==glp_get_num_int(lp));
    //glp_write_mps(lp, GLP_MPS_DECK, NULL, "api_file");
    //glp_write_lp(lp, NULL, "api_file");
    //glp_write_prob(lp, 0, "api_file");
    glp_assert(0 == glp_check_dup(pGLP->active_constraint_cnt, total_variable_cnt, pGLP->acture_element, pGLP->ith, pGLP->jth));

    glp_load_matrix(lp, pGLP->acture_element, pGLP->ith, pGLP->jth, pGLP->ij_array);
    
#if 0//for linear programing
    glp_smcp parm;
    glp_init_smcp(&parm);
    parm.presolve = GLP_ON;
    glp_simplex(lp, &parm);
    //glp_print_sol(lp, "25fv47.txt");
    //glp_write_sol(lp, "25fv48.txt");
#else//for binary integer linear programing
    glp_iocp iocpParm;
    glp_init_iocp(&iocpParm);
    iocpParm.presolve = GLP_ON;
    iocpParm.tm_lim = RUNTIME_LIMIT_SEC;
    iocpParm.msg_lev = GLP_MSG_ERR;
    glp_intopt(lp, &iocpParm);
    //glp_print_mip(lp, "shift.txt");
    //glp_write_mip(lp, "shift_s.txt");
#endif
    
    glp_output_result(lp, pGLP);
}

void glp_output_result(glp_prob *lp, tGLP* pGLP)
{
    printf("Total non-zero element %d\n", pGLP->acture_element);
    
#if 0 //retrieve column primal value
    printf("Final value %.2f\n", glp_get_obj_val(lp));
    for(int cnt=1; cnt<=get_config_total_variable_cnt(); cnt++)
        printf("%s : %.0f, %d\n", glp_get_col_name(lp, cnt), glp_get_col_prim(lp, cnt));
#else
    //retrieve objective value
    singleResult_saveValue(glp_mip_obj_val(lp));
#if 1
    for(int cnt=1; cnt<=get_config_total_variable_cnt(); cnt++)
        if(glp_mip_col_val(lp, cnt))
            printf("%s\t %.0f\n", glp_get_col_name(lp, cnt), glp_mip_col_val(lp, cnt));
#endif
#endif
}

void solution_xml(glp_prob *lp)
{
    int total_variable_cnt = get_config_total_variable_cnt();
    FILE *fb=fopen("shift.xml","w");
    fprintf ( fb,"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    fprintf ( fb,"<Data>\n");
    for(int var = 1; var <= total_variable_cnt; var++)
    {
        if(glp_mip_col_val(lp, var))
            fprintf ( fb,"<string> %s </string>\n",glp_get_col_name(lp, var));
    }
    fprintf ( fb,"</Data>\n");
}

void display_solution(glp_prob *lp, bool input, bool output)
{
    printf("\t\t [Ŕ]: Ŕed Line asked      [E]: Red Line Enhancement   [Ĺ]: Ĺong term asked\n");
    printf("\t\t [C]: Charge              [Ⱡ]: Ⱡeader                 [S]: Staff\n");
    printf("\t\t [R]: Red Line approved   [G]: Green Line approved    [L]: Long term approved\n");
    printf("\t\t [T]: Stand By            [M]: Mid Shift\n");
    int total_nurse = get_config_total_nurse();
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_s       = 0;
    int shift_e       = SHIFT_NUMBER;
    int shift_c, nurse_s = 0, nurse_c, nurse_e = 0;
    int asktotal[total_nurse];
    int hitasktotal[total_nurse];
    int offduty[total_nurse];
    int unsatisfiedCnt = 0;
    bool show_LM = false;
    
    memset(asktotal,    0, total_nurse*sizeof(asktotal[0]));
    memset(hitasktotal, 0, total_nurse*sizeof(hitasktotal[0]));
    memset(offduty,     0, total_nurse*sizeof(offduty[0]));
    
    if(total_shift == 1)
    {
        shift_s = shift->shiftCategory;
        shift_e = shift_s + 1;
    }
    dispaly_weekend();
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        display_solution_shift(shift_c, shift_nurse);
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            printf("[%2d] %5s", nurse_c, staffs[nurse_c].name);
            if(input)
            {
                display_solution_input(lp, show_LM, nurse_c, &asktotal[0]);
            }
            
            if(output)
            {
                if(input)  printf("          ");
                display_solution_output(lp, show_LM, nurse_c, &asktotal[0]);
                
                display_solution_cal_hitrate(lp, nurse_c, &offduty[0], &hitasktotal[0], &asktotal[0]);
                
                printf("\n\n");
            }
        }
        nurse_s += shift_nurse[shift_c];
    }

    for(int nurse = 0; nurse < total_nurse; nurse++)
    {
        if(hitasktotal[nurse] != asktotal[nurse])
        {
            printf("%5s  %d.%d%% (%d/%d)\t [%d]: %.2f%%\n", staffs[nurse].name, (hitasktotal[nurse]*100)/asktotal[nurse], (hitasktotal[nurse]*100)%asktotal[nurse], hitasktotal[nurse],
                asktotal[nurse], staffs[nurse].rankOfLowMSP, staffs[nurse].modifiedScoreParameter[MSP_TOTAL]);
            unsatisfiedCnt++;
        }
        singleResult_savePersonnel(&staffs[nurse], nurse, hitasktotal[nurse]);
    }
    singleResult_initPersonnel(unsatisfiedCnt);
}

void dispaly_weekend(void)
{
    int total_workday = get_config_total_workday();
    int first_day     = get_config_first_day_is();
    int cnt = 0;
    printf("          ");
    for(int day = 0; day < total_workday; day++)
    {
        if((cnt++) % 5==0)  printf("   [%2d]", cnt);
        if(is_weekend(day, first_day)) printf(" E");
        else printf("  ");
    }
    printf("\n");
}

void display_solution_shift(ShiftCategory_t shift_c, int *shift_nurse)
{
    if(shift_c == SHIFT_DAY) printf("Day Shift: %d\n", shift_nurse[shift_c]);
    else if(shift_c == SHIFT_NIGHT) printf("Night Shift: %d\n", shift_nurse[shift_c]);
    else if(shift_c == SHIFT_GRAVEYARD) printf("Graveyard Shift: %d\n", shift_nurse[shift_c]);
    else glp_assert(false);
}

void display_solution_input(glp_prob *lp, bool show_LM, int nurse_c, int* asktotal)
{
    int last_6workday_last_month = get_config_last_6workday_last_month();
    int total_workday            = get_config_total_workday();
    int cnt;
    if(show_LM)
    {
        printf("    [LM]");
        for(int day = 0; day < last_6workday_last_month; day++)
        {
            if(is_Long_off_duty(staffs[nurse_c].LMShiftList[day]))             printf(" Ĺ");
            else if(staffs[nurse_c].LMShiftList[day] == HRSTATUS_RED_OFFDUTY)  printf(" Ŕ");
            else                                                               printf(" _");
        }
        printf("  ||");
    }
    cnt=0;
    
    for(int day = 0; day < total_workday; day++)
    {
        if((cnt++) % 5==0)  printf("   [%2d]", cnt);
        display_input_HRStatus(nurse_c, day);
    }
    printf("\n");
    
    for(int day = 0; day < total_workday; day++)
    {
        if(is_off_duty(staffs[nurse_c].AskLeaveList[day]))
            asktotal[nurse_c]++;
    }
}

void display_solution_output(glp_prob *lp, bool show_LM, int nurse_c, int* asktotal)
{
    int last_6workday_last_month   = get_config_last_6workday_last_month();
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    int cnt;

    if(show_LM)
    {
        printf("    [LM]");
        for(int day = 0; day < last_6workday_last_month; day++)
        {
            for(HRStatus_t HR_Idx = 0; HR_Idx < HRSTATUS_NUMBER; HR_Idx++)
            {
                if(display_solution_HRStatus(lp, nurse_c, day, HR_Idx))
                    break;
            }
        }
        printf("  ||");
    }
    cnt=0;
    for(int day = last_6workday_last_month; day < last_month_p_total_workday; day++)
    {
        if((cnt++) % 5==0)  printf("   [%2d]", cnt);
        for(HRStatus_t HR_Idx = 0; HR_Idx < HRSTATUS_NUMBER; HR_Idx++)
        {
            if(display_solution_HRStatus(lp, nurse_c, day, HR_Idx))
                break;
        }
    }
    printf("  -");
}

void display_input_HRStatus(int nurse_c, int day)
{
    AskStatus_t askIdx;
    if(is_Long_off_duty(staffs[nurse_c].AskLeaveList[day]))             askIdx = ASKSTATUS_LONG_OFFDUTY;
    else if(is_Red_Enhance_off_duty(staffs[nurse_c].AskLeaveList[day])) askIdx = ASKSTATUS_RED_ENHANCEMENT_OFFDUTY;
    else if(is_Red_off_duty(staffs[nurse_c].AskLeaveList[day]))         askIdx = ASKSTATUS_RED_OFFDUTY;
    else if(is_Green_off_duty(staffs[nurse_c].AskLeaveList[day]))       askIdx = ASKSTATUS_GREEN_OFFDUTY;
    else                                                                askIdx = ASKSTATUS_NON;
    printf("%s", HRStatusAskPattern[askIdx]);

    singleResult_saveAskHRStatus(nurse_c, day, askIdx);
}

bool display_solution_HRStatus(glp_prob *lp, int nurse_c, int day, HRStatus_t HR_Idx)
{
    bool hit = false;
    if(glp_mip_col_val(lp, col_map(nurse_c, day, HR_Idx)))
    {
        printf("%s", HRStatusResultPattern[HR_Idx]);
        hit = true;
        int aday = get_day_minus_last_6workday_last_month(day);
        singleResult_saveResultHRStatus(nurse_c, aday, HR_Idx);
    }
    return hit;
}

void display_solution_cal_hitrate(glp_prob *lp, int nurse_c, int* offduty, int* hitasktotal, int* asktotal)
{
    int last_6workday_last_month   = get_config_last_6workday_last_month();
    int last_month_p_total_workday = get_config_last_month_p_total_workday();
    ShiftCategory_t shift_c        = staffs[nurse_c].default_shift;
    
    for(int day = last_6workday_last_month; day < last_month_p_total_workday; day++)
    {
        int day_this_month = get_day_minus_last_6workday_last_month(day);
        if(glp_mip_col_val(lp, col_map(nurse_c, day, HRSTATUS_RED_OFFDUTY)) ||
           glp_mip_col_val(lp, col_map(nurse_c, day, HRSTATUS_RED_ENHANCE_OFFDUTY)) ||
           glp_mip_col_val(lp, col_map(nurse_c, day, HRSTATUS_GREEN_OFFDUTY)) ||
           glp_mip_col_val(lp, col_map(nurse_c, day, HRSTATUS_LONG_OFFDUTY)) ||
           glp_mip_col_val(lp, col_map(nurse_c, day, get_stand_by(shift_c)))
           )
        {
            offduty[nurse_c]++;
            if(is_off_duty(staffs[nurse_c].AskLeaveList[day_this_month]))
            {
                hitasktotal[nurse_c]++;
            }
        }
    }
    if(asktotal[nurse_c]>0)
    {
        printf("  %3d%% (%2d/%2d) [%d]", (hitasktotal[nurse_c]*100)/asktotal[nurse_c], hitasktotal[nurse_c], asktotal[nurse_c], offduty[nurse_c]);
        //printf("  %d.%d%% (%d/%d) [%d]", (hitasktotal[nurse]*100)/asktotal[nurse], (hitasktotal[nurse]*100)%asktotal[nurse], hitasktotal[nurse], asktotal[nurse], offduty[nurse]);
    }
    else
        printf("               [%d]", offduty[nurse_c]);
}

void glp_debug_log(glp_prob *lp, tGLP* pGLP)
{
    glp_sort_matrix(lp);
    display_solution(lp, true, true);
    
#if 1//變數順序、名稱、目標函式中係數、上下限、種類
    int var_cnt = glp_get_num_cols(lp);
    for(int var=1; var<=var_cnt; var++)
    {
        if(glp_mip_col_val(lp, var) != 0 && glp_get_obj_coef(lp, var) == SOCRE_UNREASONABLE)
        {
            printf("%3d ", var);
            printf("%s \t", glp_get_col_name(lp, var));
            printf("coef %2.0f  %2.0f " , glp_get_obj_coef(lp, var), glp_mip_col_val(lp, var));
            int kind = glp_get_col_kind(lp, var);
            if(kind == GLP_CV)
                printf("bnd %d  kind %s", glp_get_col_type(lp, var), "continuous");
            else if(kind == GLP_IV)
                printf("bnd %d  kind %s", glp_get_col_type(lp, var), "integer");
            else if(kind == GLP_BV)
                printf("bnd %d  kind %s", glp_get_col_type(lp, var), "binary");
            printf("\n");
        }
    }
#endif

#if 0
    //i, j, ij陣列內值，各constraint內非零係數之變數
    int nurse, day, cnt;
    int idx = 1;
    int total_nurse = get_config_total_nurse();
    int total_workday = get_config_total_workday();
    int last_6workday_last_month = get_config_last_6workday_last_month();
    int cont_on_duty_limit = get_config_cont_on_duty_limit();
    
    {
        printf("work_leave \n");
        for(nurse = 0; nurse < total_nurse; nurse++)
        {
            for(day=0; day<total_workday; day++)
            {
                for(cnt=0; cnt<2; cnt++)
                {
                    glp_debug_st_log(&idx, pGLP);
                }
            }
        }
    }
    
    {
        printf("staff_demand \n");
        for(day=last_6workday_last_month; day<total_workday; day++)
        {
            for(nurse = 0; nurse < total_nurse; nurse++)
            {
                glp_debug_st_log(&idx, pGLP);
            }
        }
    }
    
    {
        printf("leader_demand \n");
        for(day=0; day<total_workday; day++)
        {
            for(nurse = 0; nurse < total_nurse; nurse++)
            {
                if(staffs[nurse].job_grade == GRADE_N1)
                {
                    glp_debug_st_log(&idx, pGLP);
                }
            }
        }
    }
    
    {
        printf("leave_quota \n");
        for(nurse = 0; nurse < total_nurse; nurse++)
        {
            for(day=0; day<total_workday; day++)
            {
                glp_debug_st_log(&idx, pGLP);
            }
            
        }
    }

    
    {
        printf("long_term_leave \n");
        for(nurse = 0; nurse < total_nurse; nurse++)
        {
            for(int day=last_6workday_last_month; day<total_workday; day++)
            {
                if(staffs[nurse].AskLeaveList[day] == DAYTYPE_VACATION_RESERVATION)
                {
                    glp_debug_st_log(&idx, pGLP);
                }
            }
        }
    }

    
    {
        printf("continuous_on_duty \n");
        for(nurse = 0; nurse < total_nurse; nurse++)
        {
            for(day=0; day<(total_workday-cont_on_duty_limit); day++)
            {
                for(cnt=day; cnt<(day+cont_on_duty_limit+1); cnt++)
                {
                    glp_debug_st_log(&idx, pGLP);
                }
            }
        }
    }
    
    {
        printf("off_duty_seperate \n");
        for(nurse = 0; nurse < total_nurse; nurse++)
        {
            for(day=0; day<total_workday-2; day++)
            {
                for(cnt=day; cnt<day+3; cnt++)
                {
                    glp_debug_st_log(&idx, pGLP);
                }
            }
        }
    }
    
    {
        printf("shift_to_shift \n");
        for(nurse = 0; nurse < total_nurse; nurse++)
        {
            for(day = last_6workday_last_month-1; day < last_6workday_last_month+1; day++)
            {
                glp_debug_st_log(&idx, pGLP);
            }
        }
    }
    
    {
        printf("night_shift_15D \n");
        for(nurse = 0; nurse < total_nurse; nurse++)
        {
            for(day = last_6workday_last_month; day < total_workday; day++)
            {
                glp_debug_st_log(&idx, pGLP);
            }
        }
    }
    
#endif
#if 0
    //constraint名稱、給定上下限、各變數之係數
    int num_row=glp_get_num_rows(lp);
    for(int cnt=1; cnt<=num_row; cnt++)
    {
        int *ind=malloc(sizeof(int)*1000);
        double *val=malloc(sizeof(double)*1000);
        int len = glp_get_mat_row(lp, cnt, ind, val);
        
        printf("%s \t", glp_get_row_name(lp, cnt));
        if(glp_get_row_ub(lp, cnt) < 9999)    printf("U %.0f ", glp_get_row_ub(lp, cnt));
        if(glp_get_row_lb(lp, cnt) > -9999)   printf("L %.0f ", glp_get_row_lb(lp, cnt));
        
        for(int cnt2=1; cnt2<=len; cnt2++)
        {
            printf("[%d] %.0f ", ind[cnt2], val[cnt2]);
        }
        printf("\n");
        free(ind);
        free(val);
    }
#endif
}
