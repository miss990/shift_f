//
//  Results.h
//  shift
//
//  Created by 張柏勛 on 2020/6/14.
//  Copyright © 2020 張柏勛. All rights reserved.
//

#ifndef Results_h
#define Results_h

#include <stdio.h>
#include "config.h"
#include "gen_glp_problem.h"


typedef struct
{
    double start;
    double end;
    double runTime;
}RunTime_t;

typedef struct
{
    const char* name;
    int giveDay;
    int askDay;
    int totalMSPRank;
    double totalMSP;
}UnSatisfied_t;

typedef struct
{
    int caseNo;
    int strategyNo;
    int totalNurse;
    int totalWorkday;
    int personnelCnt;
    double solutionValue;
    RunTime_t runTime;
    UnSatisfied_t personnelList[NURSE_ARRAY_SUPPORT];
    AskStatus_t shiftAsk[NURSE_ARRAY_SUPPORT][WORKDAY_ARRAY_SUPPORT];
    HRStatus_t shiftResult[NURSE_ARRAY_SUPPORT][WORKDAY_ARRAY_SUPPORT];
}SingleResult_t;

typedef struct
{
    SingleResult_t singleResultList[ProbCace_NUMBER][STRATEGY_CNT];
    tCONSTRAINT* backup_row_constraint_table;
}AllResult_t;

void allResult_init(void);
void allResult_free(void);
void singleResult_init(int caseIndex);
void singleResult_saveValue(double finalValue);
void singleResult_countDown(void);
void singleResult_saveTime(void);
void singleResult_saveAskHRStatus(int nurse, int day, AskStatus_t ask);
void singleResult_saveResultHRStatus(int nurse, int day, HRStatus_t hr);
void singleResult_initPersonnel(int unsatisfiedCnt);
void singleResult_savePersonnel(Staff_t* staff, int personnelIdx, int giveDay);
void singleResult_printHRStatus(void);

#endif /* Results_h */
