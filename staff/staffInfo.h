#ifndef staffinfo_h
#define staffinfo_h
#include "config.h"
#include <stdbool.h>

typedef struct
{
    const char *name;
    const char *id;
    int startWorkingYear;
    int leaveQuota_Hour;
    int askDayLeaves;        //當月總要假天數
    int askDayRedLeave;      //當月一般假要假天數
    int remainHourOwingLeave; //累積剩餘待休時數
    int remainHourAnnualLeave;//特休剩餘時數
    int completeHourAnnualLeave;//特休完整時數
    int lastmonth_shift;
    int longtermleaveask;
    int greenleaveask;
    int rankOfLowMSP;
    double modifiedScoreParameter[4];//0: askCntLeave, 1: remainHourOwingLeave, 2: remainHourAnnualLeave, 3: total
    ShiftCategory_t default_shift;
    JobPosition_t job_position;
    JobGrade_t job_grade;
    // day1 at [0], day2 at [1], ..., day12 at [11]
    //[0]~[5] represent last month working state, the record of this month start from [6]
    HRStatus_t* AskLeaveList;
    HRStatus_t* LMShiftList;
}Staff_t;

Staff_t *createEmptyStaffs(void);
void InitStaffList(Staff_t *staffs);
void setStaffName(Staff_t *staffs, int index, char *name);
void printStaffsWannaDayOffList(Staff_t staffs[], int totalStaffNum);
void SetAskLeave(Staff_t *staffs, int staffIndex, int day, HRStatus_t askDayOffType);
void SetLMShift(Staff_t *staffs, int staffIndex, int day, HRStatus_t askDayOffType);
void SetDefaultShift(Staff_t *staffs, ShiftCategory_t single_shift);
void showWannaDayOffList(Staff_t staffs, int totalDays);
void showGreenDayOffList(Staff_t staffs, int totalDays);
void showLongtermDayOffList(Staff_t staffs, int totalDays);
#endif
