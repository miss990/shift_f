#include "staffInfo.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "memory.h"
#include "share_var.h"

Staff_t* createEmptyStaffs(void)
{
    int total_nurse = get_config_total_nurse();
    Staff_t *staff = (Staff_t *)calloc(total_nurse, sizeof(Staff_t));//4
    char *nameInit = "im Test Staff";
        
    for (int i = 0; i < total_nurse; i++)
    {
        InitStaffList(staff+i);
        staff[i].name = nameInit;
    }
    return staff;
}

void InitStaffList(Staff_t *staffs)
{
    int total_workday            = get_config_total_workday();
    int last_6workday_last_month = get_config_last_6workday_last_month();
    staffs->AskLeaveList = (HRStatus_t*)calloc(total_workday, sizeof(HRStatus_t));//5
    staffs->LMShiftList  = (HRStatus_t*)calloc(last_6workday_last_month, sizeof(HRStatus_t));//6
}

void setStaffName(Staff_t *staffs, int index, char *name)
{
    staffs[index].name = name;
}

void showLongtermDayOffList(Staff_t staffs, int totalDays)
{
    bool hasLongTermLeaveRecord = false;
    int realDateOnCalendar = 1;
    int cnt = 0;
    
    printf("  Long term leave(Day): ");
    for (int day = 0; day < totalDays; day++)
    {
        if(staffs.AskLeaveList[day] == HRSTATUS_LONG_OFFDUTY)
        {
            hasLongTermLeaveRecord = true;
            realDateOnCalendar = day + 1;
            if(cnt>0)  printf(", ");
            printf("%d", realDateOnCalendar);
            cnt++;
        }
    }
    
    if(hasLongTermLeaveRecord == false)
    {
        printf("None");
    }
    else
        printf(" [%d Days]", cnt);
    printf("\n");
}

void showGreenDayOffList(Staff_t staffs, int totalDays)
{
    bool hasGreenLeaveRecord = false;
    int realDateOnCalendar = 1;
    int cnt = 0;
    
    printf("  Green leave(Day): ");
    for (int day = 0; day < totalDays; day++)
    {
        if(staffs.AskLeaveList[day] == HRSTATUS_GREEN_OFFDUTY)
        {
            hasGreenLeaveRecord = true;
            realDateOnCalendar = day + 1;
            if(cnt>0)  printf(", ");
            printf("%d", realDateOnCalendar);
            cnt++;
        }
    }
    
    if(hasGreenLeaveRecord == false)
    {
        printf("None");
    }
    else
        printf(" [%d Days]", cnt);
    printf("\n");
}

void showWannaDayOffList(Staff_t staffs, int totalDays)
{
    int cnt = 0;
    printf("  Ask enhancement leave day: ");
    for (int day = 0; day < totalDays; day++)
    {
        if (staffs.AskLeaveList[day] == HRSTATUS_RED_ENHANCE_OFFDUTY)
        {
            if(cnt>0)  printf(", ");
            printf("%d", day + 1);
            cnt++;
        }
    }
    cnt = 0;
    printf("  Ask leave days: ");
    for (int day = 0; day < totalDays; day++)
    {
        if (staffs.AskLeaveList[day] == HRSTATUS_RED_OFFDUTY)
        {
            if(cnt>0)  printf(", ");
            printf("%d", day + 1);
            cnt++;
        }
    }
    printf(" [%d Days]\n", staffs.askDayRedLeave);
    printf("  Total leave(Day): [%d Days]\n", staffs.askDayLeaves);
}

void SetAskLeave(Staff_t *staffs, int staffIndex, int day, HRStatus_t askDayOffType)
{
    day--; //day1 should be saved at [0]; day 10 should be saved at [9]
    staffs[staffIndex].AskLeaveList[day] = askDayOffType;
    if(askDayOffType == HRSTATUS_LONG_OFFDUTY)
    {
        staffs[staffIndex].askDayLeaves++;
        staffs[staffIndex].longtermleaveask++;
    }
    else if(askDayOffType == HRSTATUS_RED_OFFDUTY)
    {
        staffs[staffIndex].askDayLeaves++;
        staffs[staffIndex].askDayRedLeave++;
    }
    else if(askDayOffType == HRSTATUS_RED_ENHANCE_OFFDUTY)
    {
        staffs[staffIndex].askDayLeaves++;
        staffs[staffIndex].askDayRedLeave++;
    }
    else if(askDayOffType == HRSTATUS_GREEN_OFFDUTY)
    {
        staffs[staffIndex].askDayLeaves++;
        staffs[staffIndex].greenleaveask++;
    }
}

void SetLMShift(Staff_t *staffs, int staffIndex, int day, HRStatus_t askDayOffType)
{
    day--; //day1 should be saved at [0]; day 10 should be saved at [9]
    staffs[staffIndex].LMShiftList[day] = askDayOffType;
}

void SetDefaultShift(Staff_t *staffs, ShiftCategory_t single_shift)
{
    int* shift_nurse  = get_config_shift_nurse();
    int total_shift   = get_config_total_shift();
    int shift_s       = 0;
    int shift_e       = SHIFT_NUMBER;
    int shift_c, nurse_s = 0, nurse_c, nurse_e = 0;
    
    if(total_shift == 1)
    {
        shift_s = single_shift;
        shift_e = shift_s + 1;
    }
    
    for(shift_c = shift_s; shift_c < shift_e; shift_c++)
    {
        nurse_e += shift_nurse[shift_c];
        for(nurse_c = nurse_s; nurse_c < nurse_e; nurse_c++)
        {
            staffs[nurse_c].default_shift = shift_c;
        }
        nurse_s += shift_nurse[shift_c];
    }
}

void printStaffsWannaDayOffList(Staff_t staffs[], int totalStaffNum)
{
    int total_workday = get_config_total_workday();
    for (int i = 0; i < totalStaffNum; i++)
    {
        printf("%s:", staffs[i].name);
        showWannaDayOffList(staffs[i], total_workday);
    }
}
